<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Remove route cache
Route::get('/clear-route-cache', function() {
    $exitCode = Artisan::call('route:cache');
    return 'All routes cache has just been removed';
});
Route::get('/cache-clear', function() {
    Artisan::call('cache:clear');
     return "cache clear All";
    //dd("cache clear All");
});

Route::get('/testing', 'PagesController@testing')->name('dashboard');

//Route::group(['middleware'=>'auth'], function (){
    Route::get('/dashboard', 'PagesController@index')->name('dashboard');
    Route::get('/profile', 'PagesController@profile')->name('profile');
//});


Route::group(['prefix'=>'proses'], function (){

    Route::get('ganti-bulan-data','Proses\ProsesController@index');
    Route::get('pencairan-baru','Proses\ProsesController@pencairanBaru');

    Route::get('update-rincian-kredit','Proses\ProsesController@updateRincianKredit');
    Route::get('cek-belum-lapor','Proses\ProsesController@checkDataBelumLapor');
    Route::get('generate/filetext','Proses\ProsesController@generateFiletext');

});


Route::group(['prefix'=>'manajemen'], function (){
    //Route::group(['prefix'=>'manajemen', 'middleware' => ['role:admin']], function (){

            //debitur

        Route::get('data-debitur','ManajemenData\DebiturController@index')->name('debitur');
        Route::get('data-debitur/ajax-data','ManajemenData\DebiturController@ajaxData');
        Route::post('data-debitur/submit','ManajemenData\DebiturController@submit');
        Route::post('data-debitur/show','ManajemenData\DebiturController@show');
        Route::delete('data-debitur/delete','ManajemenData\DebiturController@destroy');

        //kredit
        //Route::get('data-kredit','ManajemenData\KreditController@index');

        //fasilitas
        Route::get('fasilitas','ManajemenData\KreditController@index');
        Route::get('fasilitas/ajax-data','ManajemenData\FasilitasController@ajaxData');
        Route::post('fasilitas/submit','ManajemenData\FasilitasController@submit');
        Route::post('fasilitas/show','ManajemenData\FasilitasController@show');
        Route::delete('fasilitas/delete','ManajemenData\FasilitasController@destroy');

        //Agunan
        Route::get('data-agunan','ManajemenData\AgunanController@index');
        Route::get('agunan/ajax-data','ManajemenData\AgunanController@ajaxData');
        Route::post('agunan/submit','ManajemenData\AgunanController@submit');
        Route::post('agunan/show','ManajemenData\AgunanController@show');
        Route::delete('agunan/delete','ManajemenData\AgunanController@destroy');

        //Penjamin
        Route::get('penjamin','ManajemenData\PenjaminController@index');
        Route::get('penjamin/ajax-data','ManajemenData\PenjaminController@ajaxData');
        Route::post('penjamin/submit','ManajemenData\PenjaminController@submit');
        Route::post('penjamin/show','ManajemenData\PenjaminController@show');
        Route::delete('penjamin/delete','ManajemenData\PenjaminController@destroy');

        //Debitur Badan Usaha
        Route::get('badanusaha','ManajemenData\DebiturBadanUsahaController@index');
        Route::get('badanusaha/ajax-data','ManajemenData\DebiturBadanUsahaController@ajaxData');
        Route::post('badanusaha/combo-parent','ManajemenData\DebiturBadanUsahaController@comboParent');
        Route::post('badanusaha/submit','ManajemenData\DebiturBadanUsahaController@submit');
        Route::post('badanusaha/show','ManajemenData\DebiturBadanUsahaController@show');
        Route::delete('badanusaha/delete','ManajemenData\DebiturBadanUsahaController@destroy');

        //pengurus badan usaha
        Route::get('pengurusbadanusaha','ManajemenData\PengurusBadanUsahaController@index');

        //dcif
        Route::get('cif','ManajemenData\DisplaycifController@index');

        //Fasilitas
        Route::get('dcif','ManajemenData\FasilitasController@index');


        //});

    });

//Route::group(['prefix'=>'utility','middleware'=>'auth'], function (){
Route::group(['prefix'=>'utility'], function (){

    //user
    Route::get('ojk','Utility\OjkController@index');
    Route::get('ojk/ajax-data','System\OjkController@ajaxData');

    //role
    Route::get('role','System\RolesController@index');
    Route::get('role/ajax-data','System\RolesController@ajaxData');

});


Route::get('/index', 'PagesController@index');


// Quick search dummy route to display html elements in search dropdown (header search)
Route::get('/quick-search', 'PagesController@quickSearch')->name('quick-search');



require __DIR__.'/auth.php';

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
