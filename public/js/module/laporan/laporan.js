"use strict";
var KTDatatablesLaporan = function() {

	var initTableLaporan = function() {
		var table = $('#tbl_laporan_mar_emar');

		// begin first table
		table.DataTable({
			"language": {
                "aria": {
                    "sortAscending": ": activate to sort column ascending",
                    "sortDescending": ": activate to sort column descending"
                },
                "emptyTable": "Data Kosong",
                "info": "Menampilkan _START_ s/d _END_ ( Total :  _TOTAL_ data)",
                "infoEmpty": "Data tidak ditemukan",
                "infoFiltered": "(filtered1 data _MAX_ total data)",
                "lengthMenu": "Menampilkan _MENU_ data",
                "search": "Cari:",
                "zeroRecords": "Data tidak ditemukan"
            },
            "order": [
                [1, 'asc']
            ],
            "lengthMenu": [
                [25, 50, 100, -1],
                [25, 50, 100, "Semua"] // change per page values here
            ],
            "pageLength": 25,
            "dom": "<'row' <'col-md-12'T>><'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r><'table-scrollable't><'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>", // horizobtal scrollable datatable
            "tableTools": {
                "sSwfPath": "../../assets/global/plugins/datatables/extensions/TableTools/swf/copy_csv_xls_pdf.swf",
                "aButtons": [{
                    "sExtends": "pdf",
                    "sButtonText": "PDF"
                }, {
                    "sExtends": "csv",
                    "sButtonText": "CSV"
                }, {
                    "sExtends": "xls",
                    "sButtonText": "Excel"
                }, {
                    "sExtends": "copy",
                    "sButtonText": "Copy"
                }]
            },
            "processing": true,
            "serverSide": true,
            "ajax": {
                "url": "reportmaremar/ajax-data",
                "type": "GET",
                "data" : function(d){
                    d.no_invoice = $('#filter_no_invoice').val();
                    
                    d.date_start = $('#filter_tgl_start').val();
                    d.date_end = $('#filter_tgl_end').val();
                    
                    d.nama_vendor = $('#filter_vendor').val();
                    d.no_kontrak = $('#filter_no_kontrak').val();

                    d.nilai_invoice_start = $('#filter_nilai_start').val();
                    d.nilai_invoice_end = $('#filter_nilai_end').val();
 
                    d.tgl_bayar_start = $('#filter_tgl_bayar_start').val();
                    d.tgl_bayar_end = $('#filter_tgl_bayar_end').val();

                    d.unit = $('#filter_unit').val();
                    d.mar_id = $('#filter_kategori_mar').val();
                    d.prk_id = $('#filter_kategori_prk').val();
                    d.status_payment = $('#filter_status_payment').val();
                    
                }
            },
            "columns": [
                {
                    "data": "id",
                    "width": "50px",
                    render: function (data, type, row, meta) {
                        return meta.row + meta.settings._iDisplayStart + 1;
                    }
                },
                { "data": "no_invoice" },
                { 
                    "data": "tgl_invoice",
                    render: function (data, type, row, meta) {
                        return tanggalIndo(data);
                    }  
                },
                { "data": "nama_vendor" },
                { "data": "no_kontrak" },
                { "data": "uraian_pekerjaan" },
                { 
                    "data": "nilai_invoice_idr", 
                    "className": "text-right",
                    render: function (data, type, row, meta) {
                        if(data>0){
                            //return data.toLocaleString('id') ;
                            return viewThousandsSeparator(data,0,0);
                        }else{
                            return '-';
                        }
                    }
                },
                { 
                    "data": "pinalty", 
                    "className": "text-right",
                    render: function (data, type, row, meta) {
                    if(data>0){
                        //return data.toLocaleString('id') ;
                        return viewThousandsSeparator(data,0,0);
                    }else{
                        return '-';
                    }
                }
                },
                { "data": "kategori_mar" },
                { "data": "kategori_prk" },
                { "data": "bidang" },
                { "data": "no_prk" },
                { "data": "unit" },
                { 
                    "data": "anggaran_operasi",
                    "className": "text-right",
                    render: function (data, type, row, meta) {
                        if(data>0){
                            //return data.toLocaleString('id') ;
                            return viewThousandsSeparator(data,0,0);
                        }else{
                            return '-';
                        }
                    }
                },
                { 
                    "data": "ako",
                    "className": "text-right",
                    render: function (data, type, row, meta) {
                        if(data>0){
                            //return data.toLocaleString('id') ;
                            return viewThousandsSeparator(data,0,0);
                        }else{
                            return '-';
                        }
                    } 
                },
                { 
                    "data": "prk_saldo_ako",
                    "className": "text-right",
                    render: function (data, type, row, meta) {
                        if(data>0){
                            //return data.toLocaleString('id') ;
                            return viewThousandsSeparator(data,0,0);
                        }else{
                            return '-';
                        }
                    } 
                },
                { 
                    "data": "tahapan_id",
                    "className": "text-center",
                    render: function (data, type, row, meta) {
                        if(data==16){
                            return "<span class='text-success'> Paid</span>";
                        }else{
                            return "<span class='text-warning'>Waiting Payment</span>";
                        }
                    } 
                },
                
            ]

		});

		
	};

	return {

		//main function to initiate the module
		init: function() {
            $.fn.dataTable.ext.errMode = 'none';
			initTableLaporan();
            $('.datatable').show();
		}
	};
}();

async function ComboKlasifikasiBiaya(parent_id=""){
    let mar_id= $('#filter_kategori_mar').val();
    let response = await fetch('reportmaremar/combo-mar', {
        method: 'POST',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        body: JSON.stringify({
            mar_id:mar_id
        })
    });
    let responseText = await response.text();
    if(responseText!=''){
        
        $('#filter_kategori_mar').html(responseText);

    }
}

function _getComboPrk(){
    let prk_id= $('#filter_kategori_prk').val();
    $.ajax({
        headers     : {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
        url        : "reportmaremar/combo-prk",
        type       : 'GET',
        contentType: false,
        cache      : false,
        processData: false,
        data       : 'prk_id='+prk_id,
        success: function(resp){
           
            $('#filter_kategori_prk').html(resp);
            
        },
        error: function(result){
            Swal.fire({
                title: "Peringatan!",
                text: result.responseText.message,
                icon: "warning",
                buttonsStyling: false,
                confirmButtonText: "Ok",
                customClass: {
                    confirmButton: "btn btn-warning"
                }
            });
        }
    });

}

jQuery(document).ready(function() {

    KTDatatablesLaporan.init();
    

    $(".btn-back-users").click(function() {
        panel("grid");
    });

    $(".btn-filter-laporan-mar-emar").click(function() {
        ComboKlasifikasiBiaya();
        _getComboPrk();
    });

    $(".btn-save-form-user").one('click',function() {
        _submitForm();
    });

    $(document).on('click', '#btn-submit-filter-laporan-mar', function() {
        $('#FilterLaporanMar').modal('hide');
        $('#tbl_laporan_mar_emar').DataTable().ajax.reload( null, false );
        
        $('#no_invoice_exp').val($('#filter_no_invoice').val());
        $('#vendor_exp').val($('#filter_vendor').val());
        $('#no_kontrak_exp').val($('#filter_no_kontrak').val());
        
        $('#start_date_exp').val($('#filter_tgl_start').val());
        $('#end_date_exp').val($('#filter_tgl_end').val());
        
        $('#nilai_invoice_start_exp').val($('#filter_nilai_start').val());
        $('#nilai_invoice_end_exp').val($('#filter_nilai_end').val());
        
        $('#tgl_bayar_start_exp').val($('#filter_tgl_bayar_start').val());
        $('#tgl_bayar_end_exp').val($('#filter_tgl_bayar_end').val());

        $('#unit_exp').val($('#filter_unit').val());
        $('#kategori_mar_exp').val($('#filter_kategori_mar').val());
        $('#prk_exp').val($('#filter_kategori_prk').val());
        $('#status_payment_exp').val($('#filter_status_payment').val());
    });
    
    
});
