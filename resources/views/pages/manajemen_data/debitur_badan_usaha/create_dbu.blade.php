<div class="panel-form-dbu" style="display: none">
    <div class="card card-custom">
        <div class="card-header flex-wrap border-1 pt-6 pb-0">
            <div class="card-title">
                <h3 class="card-label">Debitur Badan Usaha
                </h3>
            </div>
            <div class="card-toolbar">

            </div>
        </div>
        <div class="card-body">
            <form action="javascript:;" method="post" id="form-dbu" enctype="multipart/form-data">
                <input type="hidden" name="MarID" id="MarID" value=""/>
                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group row ">
                            <label class="col-sm-4 col-form-label">No. CIF <span class="text-danger">*</span></label>
                            <div class="col-sm-8">
                                <input name="no_cif" id="no_cif" type="text" class="form-control input-sm" value="">
                            </div>
                        </div>
                        <div class="form-group row ">
                            <label class="col-sm-4 col-form-label">Nama Badan Usaha <span class="text-danger">*</span></label>
                            <div class="col-sm-8">
                                <input name="nama_badan_usaha" id="nama_badan_usaha" type="text" class="form-control input-sm" value="">
                            </div>
                        </div>
                        <div class="form-group row ">
                            <label class="col-sm-4 col-form-label">No. Id Badan Usaha <span class="text-danger">*</span></label>
                            <div class="col-sm-8">
                                <input name="no_badan_usaha" id="no_badan_usaha" type="text" class="form-control input-sm" value="">
                            </div>
                        </div>
                        <div class="form-group row ">
                            <label class="col-sm-4 col-form-label">Bentuk Badan Usaha  </label>
                            <div class="col-sm-6">
                                <select name="bentuk_badan_usaha" id="bentuk_badan_usaha" class="form-control input-sm select2" style="width:100%">
                                    <option value="">Pilih Parent ....?</option>
                                    {{-- @foreach($role as $key)

                                    <option value="{{$key->id}}">{{$key->role}}</option>
                                    @endforeach --}}
                                </select>
                            </div>
                        </div>
                        <div class="form-group row ">
                            <label class="col-sm-4 col-form-label">Tempat Pendirian <span class="text-danger">*</span></label>
                            <div class="col-sm-8">
                                <input name="tempat_pendirian" id="tempat_pendirian" type="text" class="form-control input-sm" value="">
                            </div>
                        </div>
                        <div class="form-group row ">
                            <label class="col-sm-4 col-form-label">No. Akte Pendirian <span class="text-danger">*</span></label>
                            <div class="col-sm-8">
                                <input name="no_akte" id="no_akte" type="text" class="form-control input-sm" value="">
                            </div>
                        </div>
                        <div class="form-group row ">
                            <label class="col-sm-4 col-form-label">Tgl. Akte Pendirian <span class="text-danger">*</span></label>
                            <div class="col-sm-4">
                                <input name="tgl_akte" id="tgl_akte" type="text" class="form-control input-sm" value="">
                            </div>
                        </div>
                        <div class="form-group row ">
                            <label class="col-sm-4 col-form-label">Telepon <span class="text-danger">*</span></label>
                            <div class="col-sm-4">
                                <input name="telp" id="telp" type="text" class="form-control input-sm" value="">
                            </div>
                        </div>
                        <div class="form-group row ">
                            <label class="col-sm-4 col-form-label">Alamat Email <span class="text-danger">*</span></label>
                            <div class="col-sm-8">
                                <input name="alamat_email" id="alamat_email" type="text" class="form-control input-sm" value="">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-4 col-form-label">Alamat<span class="text-danger">*</span></label>
                            <div class="col-sm-8">
                                <textarea name="alamat" id="alamat"  class="form-control input-sm"></textarea>
                            </div>
                        </div>
                        <div class="form-group row ">
                            <label class="col-sm-4 col-form-label">Kelurahan <span class="text-danger">*</span></label>
                            <div class="col-sm-8">
                                <input name="kelurahan" id="kelurahan" type="text" class="form-control input-sm" value="">
                            </div>
                        </div>
                        <div class="form-group row ">
                            <label class="col-sm-4 col-form-label">Kecamatan <span class="text-danger">*</span></label>
                            <div class="col-sm-8">
                                <input name="kecamatan" id="kecamatan" type="text" class="form-control input-sm" value="">
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group row">
                            <label class="col-sm-4 col-form-label">Dati2 - Kode Pos <span class="text-danger">*</span></label>
                            <div class="col-sm-8">
                                <select name="kodepos" id="kodepos"  class="form-control input-sm">
                                    <option></option>
                                    </select>
                            </div>
                        </div>
                        <div class="form-group row ">
                            <label class="col-sm-4 col-form-label">Kode Pos <span class="text-danger">*</span></label>
                            <div class="col-sm-3">
                                <input name="kode_pos" id="kode_pos" type="text" class="form-control input-sm" value="">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-4 col-form-label">Bidang Usaha  </label>
                            <div class="col-sm-8">
                                <select name="bidang_usaha" id="bidang_usaha" class="form-control input-sm select2" style="width:100%">
                                    <option value="">Pilih ....?</option>
                                    {{-- @foreach($role as $key)

                                    <option value="{{$key->id}}">{{$key->role}}</option>
                                </div>                @endforeach --}}
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-4 col-form-label">Golongan Debitur  </label>
                            <div class="col-sm-8">
                                <select name="golongan_debitur" id="golongan_debitur" class="form-control input-sm select2" style="width:100%">
                                    <option value="">Pilih ....?</option>
                                    {{-- @foreach($role as $key)

                                    <option value="{{$key->id}}">{{$key->role}}</option>
                                </div>                @endforeach --}}
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-4 col-form-label">Go Publik  </label>
                            <div class="col-sm-4">
                                <select name="go_publik" id="go_publik" class="form-control input-sm select2" style="width:100%">
                                    <option value="">Pilih ....?</option>
                                    {{-- @foreach($role as $key)

                                    <option value="{{$key->id}}">{{$key->role}}</option>
                                </div>                @endforeach --}}
                                </select>
                            </div>
                        </div>
                        <div class="form-group row ">
                            <label class="col-sm-4 col-form-label">Peringkat Debitur <span class="text-danger">*</span></label>
                            <div class="col-sm-4">
                                <input name="peringkat_debitur" id="peringkat_debitur" type="text" class="form-control input-sm" value="">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-4 col-form-label">Lembaga Pemeringkat  </label>
                            <div class="col-sm-8">
                                <select name="lembaga_pemeringkat" id="lembaga_pemeringkat" class="form-control input-sm select2" style="width:100%">
                                    <option value="">Pilih ....?</option>
                                    {{-- @foreach($role as $key)

                                    <option value="{{$key->id}}">{{$key->role}}</option>
                                </div>                @endforeach --}}
                                </select>
                            </div>
                        </div>
                        <div class="form-group row ">
                            <label class="col-sm-4 col-form-label">Tgl. Pemeringkatan <span class="text-danger">*</span></label>
                            <div class="col-sm-8">
                                <input name="tgl_pemeringkatan" id="tgl_pemeringkatan" type="text" class="form-control input-sm" value="">
                            </div>
                        </div>
                        <div class="form-group row ">
                            <label class="col-sm-4 col-form-label">Nama Group Debitur <span class="text-danger">*</span></label>
                            <div class="col-sm-8">
                                <input name="group_debitur" id="group_debitur" type="text" class="form-control input-sm" value="">
                            </div>
                        </div>
                    </div>
                </div>


                <div class="text-right">
                    <a class="btn btn-outline-primary font-weight-bolder btn-back-mar-emar">
                        <i class="fas fa-angle-double-left"></i> Kembali
                    </a>
                    <button type="reset"  class="btn btn-default">Reset <i class="flaticon2-reload"></i></button>
                    <button type="submit"  class="btn btn-primary btn-save-form-mar-emar">Simpan <i class="flaticon-paper-plane"></i></button>
                </div>
            </form>


        </div>
    </div>
</div>
