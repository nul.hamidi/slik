{{-- Extends layout --}}
@extends('layout.default')

@section('content')

    @include('pages.manajemen_data.debitur_badan_usaha.datatable_dbu')
    @include('pages.manajemen_data.debitur_badan_usaha.create_dbu')


@endsection

{{-- Styles Section --}}
@section('styles')
    <link href="{{ asset('plugins/custom/datatables/datatables.bundle.css') }}" rel="stylesheet" type="text/css"/>
@endsection

{{-- Scripts Section --}}
@section('scripts')
    <script>
        //const url_json = "referensi/mar-emar/ajax-data";

        $(document).ready(function() {
            $('.select2').select2();
        });
    </script>

    <script src="{{ asset('plugins/custom/datatables/datatables.bundle.js') }}" type="text/javascript"></script>
    <script src="{{ asset('js/module/manajemen_data/debitur_badan_usaha.js') }}" type="text/javascript"></script>

@endsection
{{-- Content --}}



