{{-- Extends layout --}}
@extends('layout.default')

@section('content')
    @include('pages.manajemen_data.kredit.datatable_kredit')

    @include('pages.manajemen_data.kredit.create_kredit')

@endsection

{{-- Styles Section --}}
@section('styles')
    <link href="{{ asset('plugins/custom/datatables/datatables.bundle.css') }}" rel="stylesheet" type="text/css"/>
@endsection

{{-- Scripts Section --}}
@section('scripts')
    <script>
        //const url_json = "referensi/bank/ajax-data";

        $(document).ready(function() {
            $('.select2').select2();
        });
    </script>

    <script src="{{ asset('plugins/custom/datatables/datatables.bundle.js') }}" type="text/javascript"></script>
    <script src="{{ asset('js/module/manajemen_data/debitur.js') }}" type="text/javascript"></script>

@endsection
{{-- Content --}}



