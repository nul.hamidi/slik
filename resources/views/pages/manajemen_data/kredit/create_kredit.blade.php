<div class="panel-form-debitur" style="display: none">
    <div class="card card-custom">
        <div class="card-header flex-wrap border-1 pt-6 pb-0">
            <div class="card-title">
                <h3 class="card-label">Form Kredit
                </h3>
            </div>
            <div class="card-toolbar">

            </div>
        </div>
        <div class="card-body">
            <form action="javascript:;" method="post" id="form-debitur" enctype="multipart/form-data">
                <input type="hidden" name="DebiturID" id="DebiturID" value=""/>
                <div class="row">
                    <div class="col-sm-6">
                        {{-- form kiri --}}
                        <div class="form-group row">
                            <label class="col-sm-4 col-form-label">No. Rekening <span class="text-danger">*</span></label>
                            <div class="col-sm-8">
                                <input name="no_rekening" id="no_rekening" type="text" class="form-control input-sm" value="">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-4 col-form-label">No. CIF <span class="text-danger">*</span></label>
                            <div class="col-sm-8">
                                <input name="no_cif" id="no_cif" type="text" class="form-control input-sm" value="">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-4 col-form-label">Nama Debitur <span class="text-danger">*</span></label>
                            <div class="col-sm-8">
                                <input name="nama_debitur" id="nama_debitur" type="text" class="form-control input-sm" value="">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-4 col-form-label">Sifat Kredit <span class="text-danger">*</span></label>
                            <div class="col-sm-8">
                                <select name="sifat_kredit" id="sifat_kredit"  class="form-control input-sm">
                                <option></option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-4 col-form-label">Jenis Kredit <span class="text-danger">*</span></label>
                            <div class="col-sm-8">
                                <select name="jenis_kredit" id="jenis_kredit"  class="form-control input-sm">
                                <option></option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-4 col-form-label">Sistem Akad/Pembiayaan <span class="text-danger">*</span></label>
                            <div class="col-sm-8">
                                <select name="sistem_akad" id="sistem_akad"  class="form-control input-sm">
                                <option></option>
                                </select>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-sm-4 col-form-label">No. Akad / PK <span class="text-danger">*</span></label>
                            <div class="col-sm-8">
                                <input name="no_akad" id="no_akad" type="text" class="form-control input-sm" value="">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-sm-4 col-form-label">Tgl Mulai Kredit <span class="text-danger">*</span></label>
                            <div class="col-sm-8">
                                <input name="tgl_mulai" id="tgl_mulai" type="text" class="form-control input-sm" value="">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-4 col-form-label">Tgl Jatuh Tempo <span class="text-danger">*</span></label>
                            <div class="col-sm-8">
                                <input name="tgl_jatuh_tempo" id="tgl_jatuh_tempo" type="text" class="form-control input-sm" value="">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-4 col-form-label">Kode Kategori Debitur <span class="text-danger">*</span></label>
                            <div class="col-sm-8">
                                <select name="sistem_akad" id="sistem_akad"  class="form-control input-sm">
                                <option></option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-4 col-form-label">Jenis Penggunaan <span class="text-danger">*</span></label>
                            <div class="col-sm-8">
                                <select name="jenis_penggunaan" id="jenis_penggunaan"  class="form-control input-sm">
                                <option></option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-4 col-form-label">Orientasi Penggunaan <span class="text-danger">*</span></label>
                            <div class="col-sm-8">
                                <select name="jenis_penggunaan" id="jenis_penggunaan"  class="form-control input-sm">
                                <option></option>
                                </select>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-sm-4 col-form-label">Dati2 <span class="text-danger">*</span></label>
                            <div class="col-sm-8">
                                <select name="kodepos" id="kodepos"  class="form-control input-sm">
                                    <option></option>
                                    </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-4 col-form-label">Sektor Ekonomi <span class="text-danger">*</span></label>
                            <div class="col-sm-8">
                                <select name="sektor_ekonomi" id="sektor_ekonomi"  class="form-control input-sm">
                                    <option></option>
                                    </select>
                            </div>
                        </div>


                    </div>
                    <div class="col-sm-6">
                        {{-- form kanan --}}
                        <div class="form-group row">
                            <label class="col-sm-4 col-form-label">Nilai Proyek Rp. <span class="text-danger">*</span></label>
                            <div class="col-sm-8">
                                <input name="nilai_proyek" id="nilai_proyek" type="text" class="form-control input-sm" value="">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-4 col-form-label">Suku Bunga <span class="text-danger">*</span></label>
                            <div class="col-sm-3">
                                <input name="suku_bunga" id="suku_bunga" type="text" class="form-control input-sm" value="">
                            </div>
                            <div class="col-sm-3"><label class="col-sm-4 col-form-label">%</label></div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-4 col-form-label">Jenis Suku Bunga <span class="text-danger">*</span></label>
                            <div class="col-sm-8">
                                <select name="jenis_suku_bunga" id="jenis_suku_bunga"  class="form-control input-sm">
                                    <option></option>
                                    </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-4 col-form-label">Plafon Rp. <span class="text-danger">*</span></label>
                            <div class="col-sm-8">
                                <input name="plafon" id="plafon" type="text" class="form-control input-sm" value="">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-4 col-form-label">Baki Debet Rp. <span class="text-danger">*</span></label>
                            <div class="col-sm-8">
                                <input name="baki_debet" id="baki_debet" type="text" class="form-control input-sm" value="">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-4 col-form-label">Denda Rp. <span class="text-danger">*</span></label>
                            <div class="col-sm-8">
                                <input name="denda" id="denda" type="text" class="form-control input-sm" value="">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-4 col-form-label">Kolektabilitas <span class="text-danger">*</span></label>
                            <div class="col-sm-8">
                                <select name="kolektabilitas" id="kolektabilitas"  class="form-control input-sm">
                                    <option></option>
                                    </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-4 col-form-label">Frekuensi Tunggakan <span class="text-danger">*</span></label>
                            <div class="col-sm-2">
                                <input name="frekuensi_tunggakan" id="frekuensi_tunggakan" type="text" class="form-control input-sm" value="">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-4 col-form-label">Tunggakan Pokok Rp. <span class="text-danger">*</span></label>
                            <div class="col-sm-8">
                                <input name="tunggakan_pokok" id="tunggakan_pokok" type="text" class="form-control input-sm" value="">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-4 col-form-label">Tunggakan Bunga Rp. <span class="text-danger">*</span></label>
                            <div class="col-sm-2">
                                <input name="tunggakan_bunga" id="tunggakan_bunga" type="text" class="form-control input-sm" value="">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-4 col-form-label">Jumlah Hari Tunggakan <span class="text-danger">*</span></label>
                            <div class="col-sm-2">
                                <input name="hari_tunggakan" id="hari_tunggakan" type="text" class="form-control input-sm" value="">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-4 col-form-label">Tanggal Macet <span class="text-danger">*</span></label>
                            <div class="col-sm-2">
                                <input name="tgl_macet" id="tgl_macet" type="text" class="form-control input-sm" value="">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-sm-4 col-form-label">Kode Sebab Macet  </label>
                            <div class="col-sm-8">
                                <select name="kode_sebab_macet" id="kode_sebab_macet" class="form-control input-sm select2" style="width:100%">
                                    <option value="">Pilih ....?</option>
                                    {{-- @foreach($role as $key)

                                    <option value="{{$key->id}}">{{$key->role}}</option>
                                    @endforeach --}}
                                </select>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-sm-4 col-form-label">Kondisi </label>
                            <div class="col-sm-8">
                                <select name="kondisi" id="kondisi" class="form-control input-sm select2" style="width:100%">
                                    <option value="">Pilih ....?</option>
                                    {{-- @foreach($role as $key)

                                    <option value="{{$key->id}}">{{$key->role}}</option>
                                    @endforeach --}}
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-4 col-form-label">Tanggal Kondisi </label>
                            <div class="col-sm-8">
                                <select name="tgl_kondisi" id="tgl_kondisi" class="form-control input-sm select2" style="width:100%">
                                    <option value="">Pilih ....?</option>
                                    {{-- @foreach($role as $key)

                                    <option value="{{$key->id}}">{{$key->role}}</option>
                                    @endforeach --}}
                                </select>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-sm-4 col-form-label">Keterangan <span class="text-danger">*</span></label>
                            <div class="col-sm-8">
                                <input name="keterangan" id="keterangan" type="text" class="form-control input-sm" value="">
                            </div>
                        </div>

                    </div>
                </div>

                <div class="text-right">
                    <a class="btn btn-outline-primary font-weight-bolder btn-back-debitur">
                        <i class="fas fa-angle-double-left"></i> Kembali
                    </a>
                    <button type="reset"  class="btn btn-default">Reset <i class="flaticon2-reload"></i></button>
                    <button type="submit"  class="btn btn-primary btn-save-form-debitur">Simpan <i class="flaticon-paper-plane"></i></button>
                </div>
            </form>


        </div>
    </div>
</div>
