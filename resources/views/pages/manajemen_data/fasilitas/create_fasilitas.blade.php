<div class="panel-form-fasilitas" style="display: none">
    <div class="card card-custom">
        <div class="card-header flex-wrap border-1 pt-6 pb-0">
            <div class="card-title">
                <h3 class="card-label">Form Fasilitas
                </h3>
            </div>
            <div class="card-toolbar">

            </div>
        </div>
        <div class="card-body">
            <form action="javascript:;" method="post" id="form-fasilitas" enctype="multipart/form-data">
                <input type="hidden" name="DebiturID" id="DebiturID" value=""/>
                <div class="row">
                    <div class="col-sm-6">
                        {{-- form kiri --}}
                        <div class="form-group row">
                            <label class="col-sm-4 col-form-label">No. Rekening <span class="text-danger">*</span></label>
                            <div class="col-sm-8">
                                <input name="no_rek" id="no_rek" type="text" class="form-control input-sm" value="">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-4 col-form-label">No. CIF <span class="text-danger">*</span></label>
                            <div class="col-sm-8">
                                <input name="no_cif" id="no_cif" type="text" class="form-control input-sm" value="">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-4 col-form-label">Nama Debitur <span class="text-danger">*</span></label>
                            <div class="col-sm-8">
                                <input name="nama_debitur" id="nama_debitur" type="text" class="form-control input-sm" value="">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-4 col-form-label">Jenis Kredit <span class="text-danger">*</span></label>
                            <div class="col-sm-8">
                                <select name="jenis_kredit" id="jenis_kredit"  class="form-control input-sm">
                                <option></option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-4 col-form-label">Skim Akad/Pembiayaan <span class="text-danger">*</span></label>
                            <div class="col-sm-8">
                                <select name="skim_akad" id="skim_akad"  class="form-control input-sm">
                                <option></option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-4 col-form-label">No. Akad/PK <span class="text-danger">*</span></label>
                            <div class="col-sm-8">
                                <input name="no_akad" id="no_akad" type="text" class="form-control input-sm" value="">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-4 col-form-label">Tgl. Akad Kredit <span class="text-danger">*</span></label>
                            <div class="col-sm-8">
                                <input name="tgl_akad" id="tgl_akad" type="text" class="form-control input-sm" value="">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-4 col-form-label">Tgl. Mulai Kredit <span class="text-danger">*</span></label>
                            <div class="col-sm-8">
                                <input name="tgl_mulai_kredit" id="tgl_mulai_kredit" type="text" class="form-control input-sm" value="">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-4 col-form-label">Tgl. Jatuh Tempo <span class="text-danger">*</span></label>
                            <div class="col-sm-8">
                                <input name="tgl_tempo" id="tgl_tempo" type="text" class="form-control input-sm" value="">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-4 col-form-label">Tgl. Restruk Awal <span class="text-danger">*</span></label>
                            <div class="col-sm-8">
                                <input name="tgl_restruk_awal" id="tgl_restruk_awal" type="text" class="form-control input-sm" value="">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-4 col-form-label">Tgl. Restruk Akhir <span class="text-danger">*</span></label>
                            <div class="col-sm-8">
                                <input name="tgl_restruk_akhir" id="tgl_restruk_akhir" type="text" class="form-control input-sm" value="">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-4 col-form-label">>Kode Restruk<span class="text-danger">*</span></label>
                            <div class="col-sm-8">
                                <select name="kode_restruk" id="kode_restruk"  class="form-control input-sm">
                                <option></option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-4 col-form-label">Frekuensi Restruk <span class="text-danger">*</span></label>
                            <div class="col-sm-8">
                                <input name="frekuensi_restruk" id="frekuensi_restruk" type="text" class="form-control input-sm" value="">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-4 col-form-label">Keterangan <span class="text-danger">*</span></label>
                            <div class="col-sm-8">
                                <input name="Keterangan" id="Keterangan" type="text" class="form-control input-sm" value="">
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-6">
                        {{-- form kanan --}}

                        <div class="form-group row">
                            <label class="col-sm-4 col-form-label">Plafon Rp. <span class="text-danger">*</span></label>
                            <div class="col-sm-8">
                                <input name="plafon" id="plafon" type="text" class="form-control input-sm" value="">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-4 col-form-label">Bukti Debet Rp. <span class="text-danger">*</span></label>
                            <div class="col-sm-8">
                                <input name="plafon" id="plafon" type="text" class="form-control input-sm" value="">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-sm-4 col-form-label">Kolektabilitas  </label>
                            <div class="col-sm-8">
                                <select name="kolektabilitas" id="kolektabilitas" class="form-control input-sm select2" style="width:100%">
                                    <option value="">Pilih ....?</option>
                                    {{-- @foreach($role as $key)

                                    <option value="{{$key->id}}">{{$key->role}}</option>
                                    @endforeach --}}
                                </select>
                            </div>
                        </div>


                    </div>
                </div>

                <div class="text-right">
                    <a class="btn btn-outline-primary font-weight-bolder btn-back-debitur">
                        <i class="fas fa-angle-double-left"></i> Kembali
                    </a>
                    <button type="reset"  class="btn btn-default">Reset <i class="flaticon2-reload"></i></button>
                    <button type="submit"  class="btn btn-primary btn-save-form-debitur">Simpan <i class="flaticon-paper-plane"></i></button>
                </div>
            </form>


        </div>
    </div>
</div>
