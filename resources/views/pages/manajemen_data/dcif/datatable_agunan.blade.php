<div class="panel-grid-agunan">
    <div class="card card-custom">
        <div class="card-header flex-wrap border-1 pt-6 pb-0">
            <div class="card-title">
                <h3 class="card-label">Tabel Agunan</h3>
            </div>
            <div class="card-toolbar">

            </div>
        </div>
        <div class="card-body">

            <!--begin: Datatable-->
            <table class="table table-bordered " id="tbl_agunan">
                <thead>
                    <tr>
                        <th >No.</th>
                        <th >Parent</th>
                        <th >Kategory Mar/Emar</th>
                        <th width="80px" >Action</th>
                    </tr>
                </thead>
                <tbody>

                </tbody>
            </table>
        </div>
    </div>
</div>
