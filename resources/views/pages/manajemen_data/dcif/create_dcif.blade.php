<div class="panel-form-pbu">
    <div class="card card-custom">
        <div class="card-header flex-wrap border-1 pt-6 pb-0">
            <div class="card-title">
                <h3 class="card-label">Form DCIF

                </h3>
            </div>
            <div class="card-toolbar">

            </div>
        </div>
        <div class="card-body">
            <form action="javascript:;" method="post" id="form-dcif" enctype="multipart/form-data">
                <input type="hidden" name="MarID" id="MarID" value=""/>
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group row col-md-6">
                            <label class="col-sm-4 col-form-label">No. CIF <span class="text-danger">*</span></label>
                            <div class="col-sm-8">
                                <input name="no_cif" id="no_cif" type="text" class="form-control input-sm" value="">
                            </div>
                        </div>
                        <hr>


                        <div class="form-group row col-md-6">
                            <label class="col-sm-4 col-form-label">Nama Pengurus <span class="text-danger">*</span></label>
                            <div class="col-sm-8">
                                <input name="nama_pengurus" id="nama_pengurus" type="text" class="form-control input-sm" value="">
                            </div>
                        </div>
                        <div class="form-group row col-md-6">
                            <label class="col-sm-4 col-form-label">Tgl. Lahir <span class="text-danger">*</span></label>
                            <div class="col-sm-8">
                                <input name="tgl_lahir" id="tgl_lahir" type="text" class="form-control input-sm" value="">
                            </div>
                        </div>
                        <div class="form-group row col-md-6">
                            <label class="col-sm-4 col-form-label">No. KTP <span class="text-danger">*</span></label>
                            <div class="col-sm-8">
                                <input name="no_ktp" id="no_ktp" type="text" class="form-control input-sm" value="">
                            </div>
                        </div>
                        <div class="form-group row col-md-6">
                            <label class="col-sm-4 col-form-label">Alamat <span class="text-danger">*</span></label>
                            <div class="col-sm-8">
                                <input name="alamat" id="alamat" type="text" class="form-control input-sm" value="">
                            </div>
                        </div>
                        <div class="form-group row col-md-6">
                            <label class="col-sm-4 col-form-label">Kelurahan <span class="text-danger">*</span></label>
                            <div class="col-sm-8">
                                <input name="kelurahan" id="kelurahan" type="text" class="form-control input-sm" value="">
                            </div>
                        </div>
                        <div class="form-group row col-md-6">
                            <label class="col-sm-4 col-form-label">Kecamatan <span class="text-danger">*</span></label>
                            <div class="col-sm-8">
                                <input name="kecamatan" id="kecamatan" type="text" class="form-control input-sm" value="">
                            </div>
                        </div>

                        <div class="form-group row col-md-6">
                            <label class="col-sm-4 col-form-label">Dati2 - Kode Pos <span class="text-danger">*</span></label>
                            <div class="col-sm-8">
                                <select name="kodepos" id="kodepos"  class="form-control input-sm">
                                    <option></option>
                                    </select>
                            </div>
                        </div>
                        <div class="form-group row col-md-6">
                            <label class="col-sm-4 col-form-label">Kode POS <span class="text-danger">*</span></label>
                            <div class="col-sm-8">
                                <input name="kode_pos" id="kode_pos" type="text" class="form-control input-sm" value="">
                            </div>
                        </div>

                        <div class="">
                            <button type="reset"  class="btn btn-default">Reset <i class="flaticon2-reload"></i></button>
                            <button type="submit"  class="btn btn-primary btn-save-form-mar-emar">Simpan <i class="flaticon-paper-plane"></i></button>
                        </div>

                    </div>


                </div>

            </form>


        </div>
    </div>
</div>
