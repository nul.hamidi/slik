<div class="panel-form-pbu">
    <div class="card card-custom">
        <div class="card-header flex-wrap border-1 pt-6 pb-0">
            <div class="card-title">
                <h3 class="card-label">Pengurus Badan Usaha
                </h3>
            </div>
            <div class="card-toolbar">

            </div>
        </div>
        <div class="card-body">
            <form action="javascript:;" method="post" id="form-pbu" enctype="multipart/form-data">
                <input type="hidden" name="MarID" id="MarID" value=""/>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group row ">
                            <label class="col-sm-4 col-form-label">No. CIF <span class="text-danger">*</span></label>
                            <div class="col-sm-8">
                                <input name="no_cif" id="no_cif" type="text" class="form-control input-sm" value="">
                            </div>
                        </div>
                        <div class="form-group row" >
                            <label class="col-sm-4 col-form-label">Jenis Kelamin <span class="text-danger">*</span></label>
                            <div class="col-sm-8 radio-inline">

                                <label class="radio radio-primary radio_status radio_statusYes">
                                <input value="laki-laki" type="radio" name="jenis_kelamin"  id="jenis_kelamin1"/>
                                    <span></span>Laki-laki</label>
                                <label class="radio radio-primary radio_status radio_statusNo">
                                <input value="perempuan" type="radio" name="jenis_kelamin"  id="jenis_kelamin0" />
                                    <span></span>Perempuan
                                </label>

                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-4 col-form-label">Nama Pengurus <span class="text-danger">*</span></label>
                            <div class="col-sm-8">
                                <input name="nama_pengurus" id="nama_pengurus" type="text" class="form-control input-sm" value="">
                            </div>
                        </div>
                        <div class="form-group row ">
                            <label class="col-sm-4 col-form-label">No. KTP <span class="text-danger">*</span></label>
                            <div class="col-sm-8">
                                <input name="no_ktp" id="no_ktp" type="text" class="form-control input-sm" value="">
                            </div>
                        </div>
                        <div class="form-group row ">
                            <label class="col-sm-4 col-form-label">Alamat <span class="text-danger">*</span></label>
                            <div class="col-sm-8">
                                <input name="alamat" id="alamat" type="text" class="form-control input-sm" value="">
                            </div>
                        </div>
                        <div class="form-group row ">
                            <label class="col-sm-4 col-form-label">Kelurahan <span class="text-danger">*</span></label>
                            <div class="col-sm-8">
                                <input name="kelurahan" id="kelurahan" type="text" class="form-control input-sm" value="">
                            </div>
                        </div>
                        <div class="form-group row ">
                            <label class="col-sm-4 col-form-label">Kecamatan <span class="text-danger">*</span></label>
                            <div class="col-sm-8">
                                <input name="kecamatan" id="kecamatan" type="text" class="form-control input-sm" value="">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-sm-4 col-form-label">Dati2 - Kode Pos <span class="text-danger">*</span></label>
                            <div class="col-sm-8">
                                <select name="kodepos" id="kodepos"  class="form-control input-sm">
                                    <option></option>
                                    </select>
                            </div>
                        </div>
                        <div class="form-group row ">
                            <label class="col-sm-4 col-form-label">Pangsa Kepemilikan <span class="text-danger">*</span></label>
                            <div class="col-sm-4">
                                <input name="pangsa_kepemilikan" id="pangsa_kepemilikan" type="text" class="form-control input-sm" value="">
                            </div>
                            <div class="col-sm-4"><label class="col-form-label">%</label></div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-4 col-form-label">Status  </label>
                            <div class="col-sm-4">
                                <select name="go_publik" id="go_publik" class="form-control input-sm select2" style="width:100%">
                                    <option value="">Pilih ....?</option>
                                    {{-- @foreach($role as $key)

                                    <option value="{{$key->id}}">{{$key->role}}</option>
                                </div>                @endforeach --}}
                                </select>
                            </div>
                        </div>

                        <div class="">
                            <button type="reset"  class="btn btn-default">Reset <i class="flaticon2-reload"></i></button>
                            <button type="submit"  class="btn btn-primary btn-save-form-mar-emar">Simpan <i class="flaticon-paper-plane"></i></button>
                        </div>

                    </div>


                </div>

            </form>


        </div>
    </div>
</div>
