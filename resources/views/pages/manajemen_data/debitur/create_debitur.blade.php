<div class="panel-form-debitur" style="display: none">
    <div class="card card-custom">
        <div class="card-header flex-wrap border-1 pt-6 pb-0">
            <div class="card-title">
                <h3 class="card-label">Form Debitur
                </h3>
            </div>
            <div class="card-toolbar">

            </div>
        </div>
        <div class="card-body">
            <form action="javascript:;" method="post" id="form-debitur" enctype="multipart/form-data">
                <input type="hidden" name="DebiturID" id="DebiturID" value=""/>
                <div class="row">
                    <div class="col-sm-6">
                        {{-- form kiri --}}
                        <div class="form-group row">
                            <label class="col-sm-4 col-form-label">No. CIF <span class="text-danger">*</span></label>
                            <div class="col-sm-8">
                                <input name="no_cif" id="no_cif" type="text" class="form-control input-sm" value="">
                            </div>
                        </div>
                        <div class="form-group row" >
                            <label class="col-sm-4 col-form-label">Jenis Kelamin <span class="text-danger">*</span></label>
                            <div class="col-sm-8 radio-inline">

                                    <label class="radio radio-primary radio_status radio_statusYes">
                                    <input value="laki-laki" type="radio" name="jenis_kelamin"  id="jenis_kelamin1"/>
                                        <span></span>Laki-laki</label>
                                    <label class="radio radio-primary radio_status radio_statusNo">
                                    <input value="perempuan" type="radio" name="jenis_kelamin"  id="jenis_kelamin0" />
                                        <span></span>Perempuan
                                    </label>

                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-4 col-form-label">Nama Sesuai KTP <span class="text-danger">*</span></label>
                            <div class="col-sm-8">
                                <input name="nama_ktp" id="nama_ktp" type="text" class="form-control input-sm" value="">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-sm-4 col-form-label">Nama Lengkap <span class="text-danger">*</span></label>
                            <div class="col-sm-8">
                                <input name="nama_lengkap" id="nama_lengkap" type="text" class="form-control input-sm" value="">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-4 col-form-label">Tgl Lahir <span class="text-danger">*</span></label>
                            <div class="col-sm-8">
                                <input name="tgl_lahir" id="tgl_lahir" type="text" class="form-control input-sm" value="">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-4 col-form-label">No KTP <span class="text-danger">*</span></label>
                            <div class="col-sm-8">
                                <input name="no_ktp" id="no_ktp" type="text" class="form-control input-sm" value="">
                            </div>
                        </div>
                        <div class="form-group row" >
                            <label class="col-sm-4 col-form-label">Status Perkawinan <span class="text-danger">*</span></label>
                            <div class="col-sm-8 radio-inline">

                                    <label class="radio radio-primary radio_status radio_statusbelum">
                                    <input value="belum menikah" type="radio" name="status_perkawinan"  id="status_perkawinan0"/>
                                        <span></span>Belum Menikah</label>
                                    <label class="radio radio-primary radio_status radio_statusmenikah">
                                    <input value="menikah" type="radio" name="status_perkawinan"  id="status_perkawinan1" />
                                        <span></span>Menikah
                                    </label>
                                    <label class="radio radio-primary radio_status radio_statussingle">
                                    <input value="duda/janda" type="radio" name="status_perkawinan"  id="status_perkawinan2" />
                                        <span></span>Duda/Janda
                                    </label>

                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-4 col-form-label">Pendidikan <span class="text-danger">*</span></label>
                            <div class="col-sm-8">
                                <select name="pendidikan" id="pendidikan"  class="form-control input-sm">
                                <option></option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-4 col-form-label">Alamat <span class="text-danger">*</span></label>
                            <div class="col-sm-8">
                                <textarea name="alamat" id="alamat"  class="form-control input-sm"></textarea>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-4 col-form-label">Kelurahan <span class="text-danger">*</span></label>
                            <div class="col-sm-8">
                                <input name="kelurahan" id="kelurahan" type="text" class="form-control input-sm" value="">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-4 col-form-label">Kecamatan <span class="text-danger">*</span></label>
                            <div class="col-sm-8">
                                <input name="kecamatan" id="kecamatan" type="text" class="form-control input-sm" value="">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-4 col-form-label">Dati2 - Kode Pos <span class="text-danger">*</span></label>
                            <div class="col-sm-8">
                                <select name="kodepos" id="kodepos"  class="form-control input-sm">
                                    <option></option>
                                    </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-4 col-form-label">Telepon Rumah <span class="text-danger">*</span></label>
                            <div class="col-sm-8">
                                <input name="telp" id="telp" type="text" class="form-control input-sm" value="">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-4 col-form-label">No Handphone <span class="text-danger">*</span></label>
                            <div class="col-sm-8">
                                <input name="nohp" id="nohp" type="text" class="form-control input-sm" value="">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-4 col-form-label">Nama Ibu Kandung <span class="text-danger">*</span></label>
                            <div class="col-sm-8">
                                <input name="ibukandung" id="ibukandung" type="text" class="form-control input-sm" value="">
                            </div>
                        </div>

                    </div>
                    <div class="col-sm-6">
                        {{-- form kanan --}}
                        <div class="form-group row">
                            <label class="col-sm-4 col-form-label">Alamat Email <span class="text-danger">*</span></label>
                            <div class="col-sm-8">
                                <input name="alamat_email" id="alamat_email" type="text" class="form-control input-sm" value="">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-sm-4 col-form-label">Sandi Pekerjaan  </label>
                            <div class="col-sm-8">
                                <select name="sandi_pekerjaan" id="sandi_pekerjaan" class="form-control input-sm select2" style="width:100%">
                                    <option value="">Pilih ....?</option>
                                    {{-- @foreach($role as $key)

                                    <option value="{{$key->id}}">{{$key->role}}</option>
                                    @endforeach --}}
                                </select>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-sm-4 col-form-label">Tempat Bekerja <span class="text-danger">*</span></label>
                            <div class="col-sm-8">
                                <input name="tempat_bekerja" id="tempat_bekerja" type="text" class="form-control input-sm" value="">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-4 col-form-label">Bidang Usaha  </label>
                            <div class="col-sm-8">
                                <select name="bidang_usaha" id="bidang_usaha" class="form-control input-sm select2" style="width:100%">
                                    <option value="">Pilih ....?</option>
                                    {{-- @foreach($role as $key)

                                    <option value="{{$key->id}}">{{$key->role}}</option>
                                </div>                @endforeach --}}
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-4 col-form-label">Alamat Bekerja<span class="text-danger">*</span></label>
                            <div class="col-sm-8">
                                <textarea name="alamat_bekerja" id="alamat_bekerja"  class="form-control input-sm"></textarea>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-4 col-form-label">NPWP <span class="text-danger">*</span></label>
                            <div class="col-sm-8">
                                <input name="npwp" id="npwp" type="text" class="form-control input-sm" value="">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-4 col-form-label">Penghasilan/Tahun Rp. <span class="text-danger">*</span></label>
                            <div class="col-sm-8">
                                <input name="penghasilan" id="npwp" type="penghasilan" class="form-control input-sm" value="">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-4 col-form-label">Kode Penghasilan  </label>
                            <div class="col-sm-8">
                                <select name="kode_penghasilan" id="kode_penghasilan" class="form-control input-sm select2" style="width:100%">
                                    <option value="">Pilih ....?</option>
                                    {{-- @foreach($role as $key)

                                    <option value="{{$key->id}}">{{$key->role}}</option>
                                    @endforeach --}}
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-4 col-form-label">Jumlah Tanggungan <span class="text-danger">*</span></label>
                            <div class="col-sm-2">
                                <input name="jml_tanggungan" id="jml_tanggungan" type="text" class="form-control input-sm" value="">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-4 col-form-label">Nama Pasangan <span class="text-danger">*</span></label>
                            <div class="col-sm-8">
                                <input name="nama_pasangan" id="nama_pasangan" type="text" class="form-control input-sm" value="">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-4 col-form-label">Tanggal Lahir Pasangan <span class="text-danger">*</span></label>
                            <div class="col-sm-8">
                                <input name="tgl_pasangan" id="tgl_pasangan" type="text" class="form-control input-sm" value="">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-4 col-form-label">No. KTP Pasangan <span class="text-danger">*</span></label>
                            <div class="col-sm-8">
                                <input name="ktp_pasangan" id="ktp_pasangan" type="text" class="form-control input-sm" value="">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-4 col-form-label">Ada Perjanjian Pisah Harta <span class="text-danger">*</span></label>
                            <div class="col-sm-8">
                                <input name="perjanjian_harta" id="perjanjian_harta" type="text" class="form-control input-sm" value="">
                            </div>
                        </div>debitur

                    </div>
                </div>

                <div class="text-right">
                    <a class="btn btn-outline-primary font-weight-bolder btn-back-debitur">
                        <i class="fas fa-angle-double-left"></i> Kembali
                    </a>
                    <button type="reset"  class="btn btn-default">Reset <i class="flaticon2-reload"></i></button>
                    <button type="submit"  class="btn btn-primary btn-save-form-debitur">Simpan <i class="flaticon-paper-plane"></i></button>
                </div>
            </form>


        </div>
    </div>
</div>
