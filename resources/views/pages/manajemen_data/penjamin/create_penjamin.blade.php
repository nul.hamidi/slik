<div class="panel-form-penjamin" style="display: none">
    <div class="card card-custom">
        <div class="card-header flex-wrap border-1 pt-6 pb-0">
            <div class="card-title">
                <h3 class="card-label">Form Penjamin
                </h3>
            </div>
            <div class="card-toolbar">

            </div>
        </div>
        <div class="card-body">
            <form action="javascript:;" method="post" id="form-penjamin" enctype="multipart/form-data">
                <div class="form-group row col-md-7">
                    <label class="col-sm-4 col-form-label">No. Rekening <span class="text-danger">*</span></label>
                    <div class="col-sm-8">
                        <input name="no_rekening" id="no_rekening" type="text" class="form-control input-sm" value="">
                    </div>
                </div>
                <div class="form-group row col-md-7">
                    <label class="col-sm-4 col-form-label">No. CIF <span class="text-danger">*</span></label>
                    <div class="col-sm-8">
                        <input name="no_cif" id="no_cif" type="text" class="form-control input-sm" value="">
                    </div>
                </div>
                <div class="form-group row col-md-7">
                    <label class="col-sm-4 col-form-label">No. KTP <span class="text-danger">*</span></label>
                    <div class="col-sm-8">
                        <input name="no_ktp" id="no_ktp" type="text" class="form-control input-sm" value="">
                    </div>
                </div>
                <div class="form-group row col-md-7">
                    <label class="col-sm-4 col-form-label">Nama Lengkap <span class="text-danger">*</span></label>
                    <div class="col-sm-8">
                        <input name="nama_lengkap" id="nama_lengkap" type="text" class="form-control input-sm" value="">
                    </div>
                </div>
                <div class="form-group row col-md-7">
                    <label class="col-sm-4 col-form-label">Golongan Penjamin </label>
                    <div class="col-sm-8">
                        <select name="golongan_penjamin" id="golongan_penjamin" class="form-control input-sm select2" style="width:100%">
                            <option value="">Pilih ....?</option>
                            {{-- @foreach($role as $key)

                            <option value="{{$key->id}}">{{$key->role}}</option>
                            @endforeach --}}
                        </select>
                    </div>
                </div>
                <div class="form-group row col-md-7">
                    <label class="col-sm-4 col-form-label">Alamat<span class="text-danger">*</span></label>
                    <div class="col-sm-8">
                        <textarea name="alamat" id="alamat"  class="form-control input-sm"></textarea>
                    </div>
                </div>
                <div class="form-group row col-md-7">
                    <label class="col-sm-4 col-form-label">Presentase Dijamin <span class="text-danger">*</span></label>
                    <div class="col-sm-2">
                        <input name="presentase_dijamin" id="presentase_dijamin" type="text" class="form-control input-sm" value="">
                    </div>
                    <div class="col-sm-3"><label class="col-form-label">%</label></div>

                </div>

                <div class="text-right">
                    <a class="btn btn-outline-primary font-weight-bolder btn-back-klasifikasi-invoice">
                        <i class="fas fa-angle-double-left"></i> Kembali
                    </a>
                    <button type="reset"  class="btn btn-default">Reset <i class="flaticon2-reload"></i></button>
                    <button type="submit"  class="btn btn-primary btn-save-form-agunan">Simpan <i class="flaticon-paper-plane"></i></button>
                </div>
            </form>


        </div>
    </div>
</div>
