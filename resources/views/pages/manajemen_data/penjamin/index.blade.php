{{-- Extends layout --}}
@extends('layout.default')

@section('content')
    @include('pages.manajemen_data.penjamin.datatable_penjamin')

    @include('pages.manajemen_data.penjamin.create_penjamin')

@endsection

{{-- Styles Section --}}
@section('styles')
    <link href="{{ asset('plugins/custom/datatables/datatables.bundle.css') }}" rel="stylesheet" type="text/css"/>
@endsection

{{-- Scripts Section --}}
@section('scripts')
    <script>
        //const url_json = "referensi/klasifikasi-invoice/ajax-data";


            //const url_json = "referensi/bank/ajax-data";

            $(document).ready(function() {
                $('.select2').select2();
            });
    </script>


    <script src="{{ asset('plugins/custom/datatables/datatables.bundle.js') }}" type="text/javascript"></script>
    <script src="{{ asset('js/module/manajemen_data/penjamin.js') }}" type="text/javascript"></script>

@endsection
{{-- Content --}}



