<div class="panel-form-agunan" style="display: none">
    <div class="card card-custom">
        <div class="card-header flex-wrap border-1 pt-6 pb-0">
            <div class="card-title">
                <h3 class="card-label">Form Agunan
                </h3>
            </div>
            <div class="card-toolbar">

            </div>
        </div>
        <div class="card-body">
            <form action="javascript:;" method="post" id="form-agunan" enctype="multipart/form-data">
                <div class="form-group row col-md-7">
                    <label class="col-sm-4 col-form-label">No. Rekening <span class="text-danger">*</span></label>
                    <div class="col-sm-8">
                        <input name="no_rekening" id="no_rekening" type="text" class="form-control input-sm" value="">
                    </div>
                </div>
                <div class="form-group row col-md-7">
                    <label class="col-sm-4 col-form-label">No. CIF <span class="text-danger">*</span></label>
                    <div class="col-sm-8">
                        <input name="no_cif" id="no_cif" type="text" class="form-control input-sm" value="">
                    </div>
                </div>
                <div class="form-group row col-md-7">
                    <label class="col-sm-4 col-form-label">Nama Pemilik Agunan <span class="text-danger">*</span></label>
                    <div class="col-sm-8">
                        <input name="nama_agunan" id="nama_agunan" type="text" class="form-control input-sm" value="">
                    </div>
                </div>
                <div class="form-group row col-md-7">
                    <label class="col-sm-4 col-form-label">Register Agunan <span class="text-danger">*</span></label>
                    <div class="col-sm-8">
                        <input name="register_agunan" id="register_agunan" type="text" class="form-control input-sm" value="">
                    </div>
                </div>
                <div class="form-group row col-md-7">
                    <label class="col-sm-4 col-form-label">Bukti Kepemilikan <span class="text-danger">*</span></label>
                    <div class="col-sm-8">
                        <input name="bukti_kepemilikan" id="bukti_kepemilikan" type="text" class="form-control input-sm" value="">
                    </div>
                </div>
                <div class="form-group row col-md-7">
                    <label class="col-sm-4 col-form-label">Status Agunan  </label>
                    <div class="col-sm-8">
                        <select name="status_agunan" id="status_agunan" class="form-control input-sm select2" style="width:100%">
                            <option value="">Pilih ....?</option>
                            {{-- @foreach($role as $key)

                            <option value="{{$key->id}}">{{$key->role}}</option>
                            @endforeach --}}
                        </select>
                    </div>
                </div>
                <div class="form-group row col-md-7">
                    <label class="col-sm-4 col-form-label">Jenis Agunan  </label>
                    <div class="col-sm-8">
                        <select name="jenis_agunan" id="jenis_agunan" class="form-control input-sm select2" style="width:100%">
                            <option value="">Pilih ....?</option>
                            {{-- @foreach($role as $key)

                            <option value="{{$key->id}}">{{$key->role}}</option>
                            @endforeach --}}
                        </select>
                    </div>
                </div>
                <div class="form-group row col-md-7">
                    <label class="col-sm-4 col-form-label">Jenis Pengikatan  </label>
                    <div class="col-sm-8">
                        <select name="jenis_pengikatan" id="jenis_pengikatan" class="form-control input-sm select2" style="width:100%">
                            <option value="">Pilih ....?</option>
                            {{-- @foreach($role as $key)

                            <option value="{{$key->id}}">{{$key->role}}</option>
                            @endforeach --}}
                        </select>
                    </div>
                </div>
                <div class="form-group row col-md-7">
                    <label class="col-sm-4 col-form-label">Tgl. Pengkatan <span class="text-danger">*</span></label>
                    <div class="col-sm-8">
                        <input name="tgl_pengkatan" id="tgl_pengkatan" type="text" class="form-control input-sm" value="">
                    </div>
                </div>
                <div class="form-group row col-md-7">
                    <label class="col-sm-4 col-form-label">Alamat Agunan<span class="text-danger">*</span></label>
                    <div class="col-sm-8">
                        <textarea name="alamat_agunan" id="alamat_agunan"  class="form-control input-sm"></textarea>
                    </div>
                </div>
                <div class="form-group row col-md-7">
                    <label class="col-sm-4 col-form-label">Dati2 <span class="text-danger">*</span></label>
                    <div class="col-sm-8">
                        <select name="kodepos" id="kodepos"  class="form-control input-sm">
                            <option></option>
                            </select>
                    </div>
                </div>
                <div class="form-group row col-md-7">
                    <label class="col-sm-4 col-form-label">Nilai Agunan Sesuai NJOP <span class="text-danger">*</span></label>
                    <div class="col-sm-8">
                        <input name="nilai_agunan_njop" id="nilai_agunan_njop" type="text" class="form-control input-sm" value="">
                    </div>
                </div>
                <div class="form-group row col-md-7">
                    <label class="col-sm-4 col-form-label">Nilai Agunan Sesuai NJOP <span class="text-danger">*</span></label>
                    <div class="col-sm-8">
                        <input name="nilai_agunan_njop" id="nilai_agunan_njop" type="text" class="form-control input-sm" value="">
                    </div>
                </div>
                <div class="form-group row col-md-7">
                    <label class="col-sm-4 col-form-label">Nilai Agunan Menurut Bank <span class="text-danger">*</span></label>
                    <div class="col-sm-8">
                        <input name="nilai_agunan_bank" id="nilai_agunan_bank" type="text" class="form-control input-sm" value="">
                    </div>
                </div>
                <div class="form-group row col-md-7">
                    <label class="col-sm-4 col-form-label">Tgl Penilaian <span class="text-danger">*</span></label>
                    <div class="col-sm-8">
                        <input name="tgl_penilaian" id="tgl_penilaian" type="text" class="form-control input-sm" value="">
                    </div>
                </div>
                <div class="form-group row col-md-7">
                    <label class="col-sm-4 col-form-label">Nilai Agunan Menurut Penilai<span class="text-danger">*</span></label>
                    <div class="col-sm-8">
                        <input name="nilai_penilai" id="nilai_penilai" type="text" class="form-control input-sm" value="">
                    </div>
                </div>
                <div class="form-group row col-md-7">
                    <label class="col-sm-4 col-form-label">Tgl Penilaian <span class="text-danger">*</span></label>
                    <div class="col-sm-8">
                        <input name="tgl_penilaian" id="tgl_penilaian" type="text" class="form-control input-sm" value="">
                    </div>
                </div>
                <div class="form-group row col-md-7">
                    <label class="col-sm-4 col-form-label">Nama Penilai <span class="text-danger">*</span></label>
                    <div class="col-sm-8">
                        <input name="nama_penilai" id="nama_penilai" type="text" class="form-control input-sm" value="">
                    </div>
                </div>
                <div class="form-group row col-md-7">
                    <label class="col-sm-4 col-form-label">Agunan Diasuransikan </label>
                    <div class="col-sm-8">
                        <select name="agunan_asuransi" id="agunan_asuransi" class="form-control input-sm select2" style="width:100%">
                            <option value="">Pilih ....?</option>

                        </select>
                    </div>
                </div>
                <div class="text-right">
                    <a class="btn btn-outline-primary font-weight-bolder btn-back-klasifikasi-invoice">
                        <i class="fas fa-angle-double-left"></i> Kembali
                    </a>
                    <button type="reset"  class="btn btn-default">Reset <i class="flaticon2-reload"></i></button>
                    <button type="submit"  class="btn btn-primary btn-save-form-agunan">Simpan <i class="flaticon-paper-plane"></i></button>
                </div>
            </form>


        </div>
    </div>
</div>
