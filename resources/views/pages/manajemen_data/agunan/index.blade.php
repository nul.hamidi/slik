{{-- Extends layout --}}
@extends('layout.default')

@section('content')
    @include('pages.manajemen_data.agunan.datatable_agunan')

    @include('pages.manajemen_data.agunan.create_agunan')

@endsection

{{-- Styles Section --}}
@section('styles')
    <link href="{{ asset('plugins/custom/datatables/datatables.bundle.css') }}" rel="stylesheet" type="text/css"/>
@endsection

{{-- Scripts Section --}}
@section('scripts')
    <script>
        const url_json = "referensi/klasifikasi-invoice/ajax-data";


            //const url_json = "referensi/bank/ajax-data";

            $(document).ready(function() {
                $('.select2').select2();
            });
    </script>


    <script src="{{ asset('plugins/custom/datatables/datatables.bundle.js') }}" type="text/javascript"></script>
    <script src="{{ asset('js/module/manajemen_data/agunan.js') }}" type="text/javascript"></script>

@endsection
{{-- Content --}}



