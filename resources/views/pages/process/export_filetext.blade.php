{{-- Extends layout --}}
@extends('layout.default')

@section('content')
    <div class="card card-custom">
        <div class="card-header flex-wrap border-1 pt-6 pb-0">
            <div class="card-title">
                <h3 class="card-label">{{$page_title}}
                </h3>
            </div>
            <div class="card-toolbar">

            </div>
        </div>
        <div class="card-body" style="z-index:9999999999999">
            <form action="javascript:;" method="post" id="form-ganti-bulan-data" enctype="multipart/form-data">
                <div class="form-group row">
                    <div class="col-sm-12" >
                        <div class="text-warning text-justify">
                            KET : HANYA DEBITUR AKTIF DAN LUNAS YANG DI BULAN DATA YANG DIKIRIM
                        </div>
                        
                    </div>
                </div>
                <div class="form-group row ">
                    <label class="col-sm-2 col-form-label">Bulan - Tahun Data</label>
                    <div class="col-sm-4">
                        <input name="tahun" id="tahun" type="text" class="form-control input-sm kt_datepicker_month" value="">
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-sm-2" >
                    </div>
                    <div class="col-sm-4" >
                       <table width="100%" class="border">
                            <tr >
                                <th style="border:1px solid #cccc" colspan="3" class="text-center">JUMLAH DATA</th>
                            </tr>
                            <tr>
                                <th width="100px" style="border-bottom:1px solid #cccc">DEBITUR</th>
                                <td width="5px" style="border-bottom:1px solid #cccc">:</td>
                                <td style="border-bottom:1px solid #cccc"></td>
                            </tr>
                            <tr>
                                <th style="border-bottom:1px solid #cccc">FASILITAS</th>
                                <td width="5px" style="border-bottom:1px solid #cccc">:</td>
                                <td style="border-bottom:1px solid #cccc"></td>
                            </tr>
                            <tr>
                                <th style="border-bottom:1px solid #cccc">AGUNAN</th>
                                <td width="5px" style="border-bottom:1px solid #cccc">:</td>
                                <td style="border-bottom:1px solid #cccc"></td>
                            </tr>
                       </table>
                    </div>
                </div>
                
                <div class="form-group row">
                    <div class="col-sm-6">
                        <hr>
                        <div class="text-right">
                            <button type="submit"  class="btn btn-primary btn-save-form-agunan">Proses <i class="flaticon-paper-plane"></i></button>
                        </div>
                    </div>
                </div>
            </form>


        </div>
    </div>


@endsection

{{-- Styles Section --}}
@section('styles')
    <link href="{{ asset('plugins/custom/datatables/datatables.bundle.css') }}" rel="stylesheet" type="text/css"/>
@endsection

{{-- Scripts Section --}}
@section('scripts')
    <script>
        const url_json = "referensi/klasifikasi-invoice/ajax-data";

            $(document).ready(function() {
                $('.select2').select2();
            });

            $('.kt_datepicker').datepicker({
               rtl: KTUtil.isRTL(),
               format: "dd-mm-yyyy",
    
            });

            $('.kt_datepicker_month').datepicker({
               rtl: KTUtil.isRTL(),
            
               format: "mm-yyyy",
                startView: "months", 
                minViewMode: "months"
            });
    </script>


    <script src="{{ asset('plugins/custom/datatables/datatables.bundle.js') }}" type="text/javascript"></script>
    <script src="{{ asset('js/pages/crud/forms/widgets/bootstrap-datepicker.js') }}" type="text/javascript"></script>

@endsection
{{-- Content --}}



