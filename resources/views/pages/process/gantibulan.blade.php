{{-- Extends layout --}}
@extends('layout.default')

@section('content')
    <div class="card card-custom">
        <div class="card-header flex-wrap border-1 pt-6 pb-0">
            <div class="card-title">
                <h3 class="card-label">GANTI BULAN DATA
                </h3>
            </div>
            <div class="card-toolbar">

            </div>
        </div>
        <div class="card-body" style="z-index:9999999999999">
            <form action="javascript:;" method="post" id="form-ganti-bulan-data" enctype="multipart/form-data">
                <div class="form-group row">
                    <label class="col-sm-2 col-form-label">Tahun </label>
                    <div class="col-sm-4">
                        <input name="no_rekening" id="no_rekening" type="text" class="form-control input-sm kt_datepicker_year" value="">
                    </div>
                </div>
                <div class="form-group row ">
                    <label class="col-sm-2 col-form-label">Bulan </label>
                    <div class="col-sm-4">
                        <input name="no_cif" id="no_cif" type="text" class="form-control input-sm kt_datepicker_month" value="">
                    </div>
                </div>
               
                
                <div class="form-group row">
                    <div class="col-sm-6">
                        <div class="text-right">
                            <button type="submit"  class="btn btn-primary btn-save-form-agunan">Proses <i class="flaticon-paper-plane"></i></button>
                        </div>
                    </div>
                </div>
            </form>


        </div>
    </div>


@endsection

{{-- Styles Section --}}
@section('styles')
    <link href="{{ asset('plugins/custom/datatables/datatables.bundle.css') }}" rel="stylesheet" type="text/css"/>
@endsection

{{-- Scripts Section --}}
@section('scripts')
    <script>
        const url_json = "referensi/klasifikasi-invoice/ajax-data";

            $(document).ready(function() {
                $('.select2').select2();
            });

            $('.kt_datepicker_year').datepicker({
               rtl: KTUtil.isRTL(),
            
               format: "yyyy",
                viewMode: "years", 
                minViewMode: "years"
            });

            $('.kt_datepicker_month').datepicker({
               rtl: KTUtil.isRTL(),
            
               format: "mm-yyyy",
                startView: "months", 
                minViewMode: "months"
            });
    </script>


    <script src="{{ asset('plugins/custom/datatables/datatables.bundle.js') }}" type="text/javascript"></script>
    <script src="{{ asset('js/pages/crud/forms/widgets/bootstrap-datepicker.js') }}" type="text/javascript"></script>

@endsection
{{-- Content --}}



