{{-- Extends layout --}}
@extends('layout.default')

@section('content')
    <div class="panel-grid-roles">
        <div class="card card-custom">
            <div class="card-header flex-wrap border-1 pt-6 pb-0">
                <div class="card-title">
                    <h3 class="card-label">Daftar Role
                    </h3>
                </div>
                <div class="card-toolbar">
                   
                   
                </div>
            </div>
            <div class="card-body" >
                <div style="overflow-x: scroll">
                <!--begin: Datatable-->
                <table class="table table-bordered " id="tbl_roles">
                    <thead>
                        <tr>
                            <th >No.</th>
                            <th >Role</th>
                            <th >Disposisi / Meneruskan</th>
                            <th >Approval</th>
                            <th >Mengembalikan Dok. Ke bawahan/Staf Lain</th>
                            <th >Mengembalikan / Konfirmasi Dok. ke user</th>
                            <th >Edit Dok./ Data Verifikasi</th>
                            <th >Edit Teks pada dokumen Input</th>
                            <th width="80px" >Action</th>
                        </tr>
                    </thead>
                    <tbody>
                    
                    </tbody>
                </table>
                </div>
            </div>
        </div>
    </div>

    <div class="panel-form-role" style="display: none">
        <div class="card card-custom">
            <div class="card-header flex-wrap border-1 pt-6 pb-0">
                <div class="card-title">
                    <h3 class="card-label">Form Update Role : <b><span id="RoleLabel"></span></b>
                    </h3>
                </div>
                <div class="card-toolbar">
                    
                </div>
            </div>
            <div class="card-body">
                <form action="javascript:;" method="post" id="form-roles" enctype="multipart/form-data">
                    <input type="hidden" name="RoleID" id="RoleID" value=""/>
                    
                    <div class="row" style="border-bottom:1px dashed #ccc;padding-bottom:5px">
                        <label class="col-sm-6"><b>Disposisi / Meneruskan ?</b></label>
                        <div class="col-sm-6">
                            <div class="radio-inline">
                                <label class="radio radio-primary ">
                                <input value="1" type="radio" name="disposisi" id="disposisi1" />
                                    <span></span>Ya</label>
                                <label class="radio radio-primary ">
                                <input value="0" type="radio" name="disposisi" id="disposisi0" />
                                    <span></span>Tidak 
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="row" style="border-bottom:1px dashed #ccc;padding-bottom:5px">
                        <label class="col-sm-6"><b>Mengembalikan Dok. Ke bawahan/Staf Lain ?</b></label>
                        <div class="col-sm-6">
                            <div class="radio-inline">
                                <label class="radio radio-primary ">
                                <input value="1" type="radio" name="kembalikan_dok" id="kembalikan_dok1"/>
                                    <span></span>Ya</label>
                                <label class="radio radio-primary ">
                                <input value="0" type="radio" name="kembalikan_dok" id="kembalikan_dok0" />
                                    <span></span>Tidak 
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="row" style="border-bottom:1px dashed #ccc;padding-bottom:5px">
                        <label class="col-sm-6"><b>Mengembalikan / Konfirmasi Dok. ke user ?</b></label>
                        <div class="col-sm-6">
                            <div class="radio-inline">
                                <label class="radio radio-primary ">
                                <input value="1" type="radio" name="konfirmasi_dok" id="konfirmasi_dok1"/>
                                    <span></span>Ya</label>
                                <label class="radio radio-primary ">
                                <input value="0" type="radio" name="konfirmasi_dok" id="konfirmasi_dok0"/> 
                                    <span></span>Tidak 
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="row" style="border-bottom:1px dashed #ccc;padding-bottom:5px">
                        <label class="col-sm-6"><b>Edit Dok./ Data Verifikasi ?</b></label>
                        <div class="col-sm-6">
                            <div class="radio-inline">
                                <label class="radio radio-primary ">
                                <input value="1" type="radio" name="edit_dok" id="edit_dok1"/>
                                    <span></span>Ya</label>
                                <label class="radio radio-primary ">
                                <input value="0" type="radio" name="edit_dok"  id="edit_dok0"/>
                                    <span></span>Tidak 
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="row" style="border-bottom:1px dashed #ccc;padding-bottom:5px">
                        <label class="col-sm-6"><b>Edit Teks pada dokumen Input ?</b></label>
                        <div class="col-sm-6">
                            <div class="radio-inline">
                                <label class="radio radio-primary ">
                                <input value="1" type="radio" name="edit_text" id="edit_text1"/>
                                    <span></span>Ya</label>
                                <label class="radio radio-primary ">
                                <input value="0" type="radio" name="edit_text" id="edit_text0" />
                                    <span></span>Tidak 
                                </label>
                            </div>
                        </div>
                    </div>
                    
                    <div class="text-right">
                        <a class="btn btn-outline-primary font-weight-bolder btn-back-roles">
                            <i class="fas fa-angle-double-left"></i> Kembali
                        </a>
                        <button type="reset"  class="btn btn-default">Reset <i class="flaticon2-reload"></i></button>
                        <button type="submit"  class="btn btn-primary btn-save-form-roles">Simpan <i class="flaticon-paper-plane"></i></button>
                    </div>
                </form>
    
             
            </div>
        </div>
    </div>

@endsection

{{-- Styles Section --}}
@section('styles')
    <link href="{{ asset('plugins/custom/datatables/datatables.bundle.css') }}" rel="stylesheet" type="text/css"/>
@endsection

{{-- Scripts Section --}}
@section('scripts')
    <script>
        const url_json = "system/role/ajax-data";
        const role_id = '{{auth()->user()->role_id}}';
    </script>
        
    <script src="{{ asset('plugins/custom/datatables/datatables.bundle.js') }}" type="text/javascript"></script>
    <script src="{{ asset('js/module/system/roles.js') }}" type="text/javascript"></script>

@endsection
{{-- Content --}}



