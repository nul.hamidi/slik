<?php

use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;

function menuAside()
{
    // $data = DB::table('sys_menu')->get();

    // $menu = [
    //     'items' => [

    //     ]
    // ];
    $menu = [
        'items' => [
            // Dashboard
            [
                'title' => 'Home',
                'root' => true,
                'icon' => 'media/svg/icons/Design/Layers.svg', // or can be 'flaticon-home' or any flaticon-*
                'page' => 'dashboard',
                'new-tab' => false,
            ],
    
            [
                'title' => 'Menu Proses',
                'icon' => 'media/svg/icons/Files/Group-folders.svg',
                'bullet' => 'line',
                'arrow' => true,
                'root' => true,
                'submenu' => [
                    [
                        'title' => 'Ganti Bulan Data',
                        'page' => 'proses/ganti-bulan-data'
                    ],
                    [
                        'title' => 'Proses Debitur dan Fasilitas (Pencairan baru)',
                        'page' => 'proses/pencairan-baru'
                    ],
                    [
                        'title' => 'Proses Update Rincian Kredit',
                        'page' => 'proses/update-rincian-kredit'
                    ],
                    [
                        'title' => 'Cek Jika Ada Data Belum Lapor (DBL/DTU)',
                        'page' => 'proses/cek-belum-lapor'
                    ],
                    [
                        'title' => 'Pembentukan FileText (.txt)',
                        'page' => 'proses/generate/filetext'
                    ],
                ]
            ],
            [
                'title' => 'Manajemen Data',
                'icon' => 'media/svg/icons/Layout/Layout-4-blocks.svg',
                'bullet' => 'line',
                'arrow' => true,
                'root' => true,
                'submenu' => [
                    [
                        'title' => 'Debitur (D01)',
                        'page' => 'manajemen/data-debitur'
                    ],
                    [
                        'title' => 'Fasilitas (F01)',
                        'page' => 'manajemen/fasilitas'
                    ],
                    [
                        'title' => 'Fasilitas (F01) Jika ada Restik',
                        'page' => 'manajemen/fasilitas-restik'
                    ],
                    [
                        'title' => 'Agunan (A01)',
                        'page' => 'manajemen/data-agunan'
                    ],
                    [
                        'title' => 'Penjamin (P01)',
                        'page' => 'manajemen/penjamin'
                    ],
                    [
                        'title' => 'Debitur Badan Usaha (D02)',
                        'page' => 'manajemen/badanusaha'
                    ],
                    [
                        'title' => 'Pengurus Debitur Badan Usaha (D02)',
                        'page' => 'manajemen/pengurusbadanusaha'
                    ],
                    [
                        'title' => 'Display Data Per CIF',
                        'page' => 'manajemen/cif'
                    ],
                ]
            ],
            [
                'title' => 'Utility',
                'icon' => 'media/svg/icons/General/Settings-1.svg',
                'bullet' => 'line',
                'arrow' => true,
                'root' => true,
                'submenu' => [
                    [
                        'title' => 'Pembentukan File Text Jika Error Validasi Server OJK',
                        'page' => 'utility/validasi-server-ojk'
                    ],
                    [
                        'title' => 'Pembentukan File Text Header ny Saja',
                        'page' => 'utility/header-saja'
                    ],
                    [
                        'title' => 'Pembentukan File Text By No. Rekening',
                        'page' => 'utility/bynorekening'
                    ],
                ]
            ],
      
        ]
    ];

    return $menu['items'];
}
