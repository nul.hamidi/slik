<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class InvoiceHistory extends Model
{
    protected $table = 'invoice_histories';
    protected $primaryKey = 'id';
    protected $fillable = [
        'invoice_id',
        'tahapan_id',
        'is_verifikasi',
        'sender',
        'recipient',
        'pengirim',
        'penerima',
        'disposisi',
        'keterangan',
        'dikembalikanke',
        'keterangan_dikembalikan',
        'created_by'
    ];

    public function invoices(){
        return $this->belongsTo(Invoice::class,'invoice_id');
	}
}
