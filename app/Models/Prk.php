<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Prk extends Model
{
    //use HasFactory;
    protected $table = 'prks';
    protected $primaryKey = 'id';
    protected $fillable = [
        'no_prk',
        'user',
        'owner',
        'kategori',
        'uraian',
        'anggaran_multiyears',
        'anggaran_operasi',
        'ako',
        'jenis',
        'tahun',
        'saldo_ako',
        'realisasi',
        'created_by',
        'updated_by'
    ];
}
