<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PajakDokumen extends Model
{
    protected $table = 'pajak_dokumens';
    protected $primaryKey = 'id';
    protected $fillable = [
        'pajak_id',
        'dokumen',
        'tipe_dok',
        'keterangan'
    ];
}
