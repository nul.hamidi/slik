<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RefBank extends Model
{
    protected $table = 'ref_banks';
    protected $primaryKey = 'id';
    protected $fillable = [
        'bank',
        'currency_id',
        'ref_currencies_id',
        'status_code'
    ];

    public function ref_currencies(){
        return $this->belongsTo(RefCurrency::class,'currency_id');
	}

}
