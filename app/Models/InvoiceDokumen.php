<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class InvoiceDokumen extends Model
{
    
    protected $table = 'invoice_dokumens';
    protected $primaryKey = 'id';
    protected $fillable = [
        'invoice_id',
        'dokumen',
        'tipe_dok',
        'keterangan'
    ];
}
