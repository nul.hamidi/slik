<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RefMarEmar extends Model
{
    protected $table = 'ref_mar_emars';
    protected $primaryKey = 'id';
    protected $fillable = [
        'parent_id',
        'label'
    ];
}
