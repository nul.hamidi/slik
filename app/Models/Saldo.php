<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Saldo extends Model
{
    protected $table = 'saldos';
    protected $primaryKey = 'id';
    protected $fillable = [
        'currency_id',
        'bank_id',
        'invoice_id',
        'pajak_id',
        'nominal',
        'saldo_akhir',
        'tanggal',
        'no_cek',
        'keterangan',
        'tipe',
        'created_by',
        'updated_by'
    ];
}
