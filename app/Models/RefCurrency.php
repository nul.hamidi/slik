<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RefCurrency extends Model
{
    protected $table = 'ref_currencies';
    protected $primaryKey = 'id';
    protected $fillable = [
        'currency',
        'status_code'
    ];

}
