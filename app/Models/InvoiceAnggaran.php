<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class InvoiceAnggaran extends Model
{
    protected $table = 'invoice_anggarans';
    protected $primaryKey = 'id';
    protected $fillable = [
        'invoice_id',
        'klasifikasi_biaya_id',
        'prk_id',
        'saldo_ako',
        'saldo',
        'unit',
        'created_by',
        'updated_by'
    ];
}
