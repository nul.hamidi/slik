<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SysRole extends Model
{
    protected $table = 'sys_roles';

    protected $fillable = [
        'role',
        'is_disposisi',
        'is_approval',
        'is_back_document',
        'is_confirm_document',
        'is_update_document',
        'is_update_text',
        'status_code'
    ];
}
