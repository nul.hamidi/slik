<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Invoice extends Model
{
    
    protected $table = 'invoices';
    protected $primaryKey = 'id';
    protected $fillable = [
        'no_invoice',
        'tgl_invoice',
        'nama_vendor',
        'no_kontrak',
        'uraian_pekerjaan',
        'currency_id',
        'kurs',
        'nilai_invoice',
        'nilai_invoice_idr',
        'dpp',
        'tarif_ppn',
        'nilai_ppn',
        'pajak_id',
        'tarif',
        'nilai_pajak',
        'pinalty',
        'netto',
        'periode',
        'sap',
        'jenis_tagihan',
        'note',
        'nama_bank',
        'no_rekening',
        'nama_rekening',
        'bank_id',
        'no_dokumen_bayar',
        'tgl_bayar',
        'no_cek',
        'tahapan_id',
        'history_id',
        'is_complete',
        'verifikator',
        'is_cancel_payment',
        'status_code',
        'created_by',
        'updated_by'
    ];

    public function currencies(){
        return $this->belongsTo(RefCurrency::class,'currency_id');
	}
}
