<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Pajak extends Model
{
    protected $table = 'pajaks';
    protected $primaryKey = 'id';
    protected $fillable = [
        'no_pajak',
        'tgl_pajak',
        'jenis_pajak_id',
        'no_doc_sap',
        'masa_pajak',
        'currency_id',
        'nilai_pajak',
        'nilai_pajak_idr',
        'keterangan',
        'no_dokumen_bayar',
        'tgl_bayar',
        'no_cek',
        'tahapan_id',
        'history_id',
        'is_complete',
        'created_by',
        'updated_by'
    ];

    public function jenispajak(){
        return $this->belongsTo(JenisPajak::class,'jenis_pajak_id');
	}
}
