<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PajakHistory extends Model
{
    protected $table = 'pajak_histories';
    protected $primaryKey = 'id';
    protected $fillable = [
        'pajak_id',
        'tahapan_id',
        'is_verifikasi',
        'sender',
        'recipient',
        'pengirim',
        'penerima',
        'disposisi',
        'keterangan',
        'dikembalikanke',
        'keterangan_dikembalikan',
        'created_by'
    ];

    public function pajaks(){
        return $this->belongsTo(Pajak::class,'pajak_id');
	}
}
