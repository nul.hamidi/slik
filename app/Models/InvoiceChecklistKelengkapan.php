<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class InvoiceChecklistKelengkapan extends Model
{
    protected $table = 'invoice_checklist_kelengkapans';
    protected $primaryKey = 'id';
    protected $fillable = [
        'invoice_id',
        'klasifikasi_invoice_id',
        'is_check',
        'keterangan'
    ];
}
