<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class KlasifikasiInvoice extends Model
{
    protected $table = 'klasifikasi_invoices';
    protected $primaryKey = 'id';
    protected $fillable = [
        'jenis_tagihan',
        'uraian'
    ];
}
