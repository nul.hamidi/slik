<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RefBeban extends Model
{
    protected $table = 'ref_bebans';
    protected $primaryKey = 'id';
    protected $fillable = [
        'parent_id',
        'label'
    ];
}
