<?php

namespace App\Http\Controllers\System;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use DataTables;

use App\Models\User;
use App\Models\SysRole;

class RolesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $page_title = 'Roles';
        return view('pages.system.role.index',compact('page_title'));

    }

    public function ajaxData(Request $request)
    {
        
        $role = SysRole::All();
        return Datatables::of($role)->make(true);
    }

    public function submit(Request $req)
    {
        try {
            $id = $req['id'];
            unset($req['id']);
            $post = array(
                'is_disposisi' => $req['is_disposisi'],
                'is_back_document' => $req['is_back_document'],
                'is_confirm_document' => $req['is_confirm_document'],
                'is_update_document' => $req['is_update_document'],
                'is_update_text' => $req['is_update_text'],
            );
            if($id!=""){
                $savaUser = SysRole::where('id',$id)->update($post);
                $lastid_ = $id;
            }else{
                $save = SysRole::create($post);
                $lastid_ = $save->id;
            }

            $return = array(
                'success' => "true",
                'message' => "Data berhasil di simpan.",
                'id' => $lastid_
            );
            return $return;
        } catch (Exception $e) {       // Rollback Transaction
            DB::rollback();
            $return = array(
                'success' => "false",
                'message' => "Terjadi Kesalahan",
                'id' => ""
            );
            return $return;
        }
    }

    public function show(Request $req)
    {
        $id = $req->id;
        $hu = SysRole::findOrfail($id);
        if(!empty($hu)){
            $return= $hu;
        }else{
            $return= array();
        }
        return $return;
    }

    public function destroy(Request $req)
    {
        $id = $req->id;
        $delete = User::where('id',$id)->delete();
        if ($delete){
            $ret = "true";
            $message = 'Data berhasil di hapus.';
        }else{
            $ret = "false";
            $message = 'Data gagal di hapus. Refresh dan coba kembali. Jika masih error hubungi Administrator.';
        }
        $return = array(
            'success' => $ret,
            'message' => $message
        );
        return $return;
    }



}
