<?php

namespace App\Http\Controllers\ManajemenData;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use DataTables;

class KreditController extends Controller
{
    //
    public function index()
    {
        $page_title = 'Kredit';

        return view('pages.manajemen_data.kredit.index',compact('page_title'));

    }

    public function ajaxData(Request $request)
    {
        $RefBank = RefBank::join('ref_currencies as cur','cur.id','ref_banks.currency_id')
                    ->select('ref_banks.id','ref_banks.bank','cur.currency')
                    ->get();
        /*$RefBank = RefBank::query()
                    ->with(['ref_currencies' => function ($query) {
                        $query->select('id', 'currency');
                    }])
                    ->get(); */
        return Datatables::of($RefBank)->make(true);
    }

    public function submit(Request $req)
    {
        try {
            $id = $req['id'];
            unset($req['id']);
            $check = RefBank::where('id',$id)->first();
            $post = array(
                'bank' => $req['nama_bank'],
                'currency_id' => $req['currency_id'],
            );
            if(!empty($check)){
                $mare = RefBank::where('id',$id)->update($post);
                $lastid_ = $id;
            }else{
                $save = RefBank::create($post);
                $lastid_ = $save->id;
            }

            $return = array(
                'success' => "true",
                'message' => "Data berhasil di simpan.",
                'id' => $lastid_
            );
            return $return;
        } catch (Exception $e) {       // Rollback Transaction
            DB::rollback();
            $return = array(
                'success' => "false",
                'message' => "Terjadi Kesalahan",
                'id' => ""
            );
            return $return;
        }
    }

    public function show(Request $req)
    {
        $id = $req->id;
        $hu = RefBank::findOrfail($id);
        if(!empty($hu)){
            $return= $hu;
        }else{
            $return= array();
        }
        return $return;
    }

    public function destroy(Request $req)
    {
        $id = $req->id;
        $delete = RefBank::where('id',$id)->delete();
        if ($delete){
            $ret = "true";
            $message = 'Data berhasil di hapus.';
        }else{
            $ret = "false";
            $message = 'Data gagal di hapus. Refresh dan coba kembali. Jika masih error hubungi Administrator.';
        }
        $return = array(
            'success' => $ret,
            'message' => $message
        );
        return $return;
    }

}
