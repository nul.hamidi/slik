<?php

namespace App\Http\Controllers\ManajemenData;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use DataTables;


class FasilitasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $page_title = 'Fasilitas';
        return view('pages.manajemen_data.fasilitas.index',compact('page_title'));

    }

    public function ajaxData(Request $request)
    {
        $RefBeb = DB::table('ref_bebans as beb')
                    ->leftJoin('ref_bebans as b','b.id','=','beb.parent_id')
                    ->select('beb.id','beb.label','b.label as parent')
                    ->get(); 
        return Datatables::of($RefBeb)->make(true);
    }

    public function comboParent(Request $request)
    {
       
        $data = array();
        $data = '<option value="">.........?</option>';
        $i=0;
        $RefMare = RefBeban::where('parent_id',0)->get();
        foreach ($RefMare as $key) {
            $i++;
            $level1 = RefBeban::where('parent_id',$key->id)->get();
            $data .= '<option value="'.$key->id.'">'.$key->label.'</option>';
            
            $i1=0;
            foreach ($level1 as $lv1) {
                $i1++;
                $level2 = RefBeban::where('parent_id',$lv1->id)->get();
                
                $data .= '<option value="'.$lv1->id.'"> &nbsp;&nbsp; '.$i1.'. '.$lv1->label.'</option>';
                
                $i2=0;
                foreach ($level2 as $lv2) {
                    $i2++;
                    $level3 = RefBeban::where('parent_id',$lv2->id)->get();

                    $data .= '<option value="'.$lv2->id.'"> &nbsp;&nbsp;&nbsp;&nbsp; '.$i1.'.'.$i2.'. '.$lv2->label.'</option>';
                    
                    $i3=0;
                    foreach ($level3 as $lv3) {
                        $i3++;
                        
                        $data .= '<option value="'.$lv3->id.'"> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; '.$i1.'.'.$i2.'.'.$i3.'. '.$lv3->label.'</option>';
                        //$level3 = RefBeban::where('parent_id',$lv2->id)->get();
                    }
                }
                
            }

        }
        echo $data;
    }

    public function comboParentVerifikasi(Request $request)
    {
        
        $data = array();
        $data = '<option value="">Pilih Beban ...</option>';
        $i=0;
        $RefMare = RefBeban::where('parent_id',0)->get();
        foreach ($RefMare as $key) {
            $i++;
            $level1 = RefBeban::where('parent_id',$key->id)->get();
            //$data .= '<option value="'.$key->id.'">'.$key->label.'</option>';
            $data .= '<optgroup label="'.$key->label.'">';
            $i1=0;
            foreach ($level1 as $lv1) {
                $i1++;
                $level2 = RefBeban::where('parent_id',$lv1->id)->get();
                if(count($level2)>0){
                    $data .= '<optgroup label="&nbsp;&nbsp; '.$i1.'. '.$lv1->label.'">';
                }else{
                    $data .= '<option value="'.$lv1->id.'"> &nbsp;&nbsp; '.$i1.'. '.$lv1->label.'</option>';
                }
                
                $i2=0;
                foreach ($level2 as $lv2) {
                    $i2++;
                    $level3 = RefBeban::where('parent_id',$lv2->id)->get();

                    if(count($level3)>0){
                        $data .= '<optgroup label="&nbsp;&nbsp; '.$i1.'.'.$i2.'. '.$lv2->label.'">';
                    }else{
                        $data .= '<option value="'.$lv2->id.'"> &nbsp;&nbsp;&nbsp;&nbsp; '.$i1.'.'.$i2.'. '.$lv2->label.'</option>';
                    }
                    
                    
                    $i3=0;
                    foreach ($level3 as $lv3) {
                        $i3++;
                        
                        $data .= '<option value="'.$lv3->id.'"> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; '.$i1.'.'.$i2.'.'.$i3.'. '.$lv3->label.'</option>';
                        //$level3 = RefBeban::where('parent_id',$lv2->id)->get();
                    }

                    if(!empty($level3)){
                        $data .= '</optgroup>';
                    }else{
                        $data .='';
                    }
                }
                if(!empty($level2)){
                    $data .= '</optgroup>';
                }else{
                    $data .='';
                }
            }
            $data .= '</optgroup>';

        }
        echo $data;
    }

    public function create()
    {
        $title = 'Beban';
        $RefMare = RefBeban::where('parent_id',0)->get();
        $data =array();
        $i=0;
        foreach ($RefMare as $key) {
            $i++;
            $data[] = $i.'.'.$key->label;
            $i1=0;
            $level1 = RefBeban::where('parent_id',$key->id)->get();
            foreach ($level1 as $lv1) {
                $i1++;
                $data[] ='-'.$i.'.'.$i1.'.'.$lv1->label;
                $level2 = RefBeban::where('parent_id',$lv1->id)->get();
                $i2=0;
                foreach ($level2 as $lv2) {
                    $i2++;
                    $data[] ='--'.$i.'.'.$i1.'.'.$i2.'.'.$lv2->label;
                    $level3 = RefBeban::where('parent_id',$lv2->id)->get();
                    $i3=0;
                    foreach ($level3 as $lv3) {
                        $i3++;
                        $data[] ='---'.$i.'.'.$i1.'.'.$i2.'.'.$i3.'.'.$lv3->label;
                        //$level3 = RefBeban::where('parent_id',$lv2->id)->get();
                    }
                }
            }

        }
        return response()->json($data);
    }

    public function submit(Request $req)
    {
        try {
            $id = $req['id'];
            unset($req['id']);
            if($req['parent_id']==""){
                $parent_id = 0;
            }else{
                $parent_id = $req['parent_id'];
            }
            $post = array(
                'parent_id' => $parent_id,
                'label' => $req['label'],
            );
            if($id!=""){
                $post['updated_by'] = auth()->user()->id;
                $mare = RefBeban::where('id',$id)->update($post);
                $lastid_ = $id;
            }else{
                $post['created_by'] =auth()->user()->id;
                $save = RefBeban::create($post);
                $lastid_ = $save->id;
            }

            $return = array(
                'success' => "true",
                'message' => "Data berhasil di simpan.",
                'id' => $lastid_
            );
            return $return;
        } catch (Exception $e) {       // Rollback Transaction
            DB::rollback();
            $return = array(
                'success' => "false",
                'message' => "Terjadi Kesalahan",
                'id' => ""
            );
            return $return;
        }
    }

    public function show(Request $req)
    {
        $id = $req->id;
        $hu = RefBeban::findOrfail($id);
        if(!empty($hu)){
            $return= $hu;
        }else{
            $return= array();
        }
        return $return;
    }

    public function destroy(Request $req)
    {
        $id = $req->id;
        $delete = RefBeban::where('id',$id)->delete();
        if ($delete){
            $ret = "true";
            $message = 'Data berhasil di hapus.';
        }else{
            $ret = "false";
            $message = 'Data gagal di hapus. Refresh dan coba kembali. Jika masih error hubungi Administrator.';
        }
        $return = array(
            'success' => $ret,
            'message' => $message
        );
        return $return;
    }



}
