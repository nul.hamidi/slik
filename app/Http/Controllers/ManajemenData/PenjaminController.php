<?php

namespace App\Http\Controllers\ManajemenData;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use DataTables;

class PenjaminController extends Controller
{
    //
    public function index()
    {
        $page_title = 'Data Penjamin';
        return view('pages.manajemen_data.penjamin.index',compact('page_title'));

    }

    public function ajaxData(Request $request)
    {
        $RefUniKompetensi = KlasifikasiInvoice::all();
        return Datatables::of($RefUniKompetensi)->make(true);
    }

    public function submit(Request $req)
    {
        try {
            $id = $req['id'];
            unset($req['id']);
            $post = array(
                'jenis_tagihan' => strtoupper($req['jenis_tagihan']),
                'uraian' => $req['uraian'],
            );
            if($id!=""){
                //$post['updated_by'] = auth()->user()->id;
                $saveHasiUji = KlasifikasiInvoice::where('id',$id)->update($post);
                $lastid_ = $id;
            }else{
                //$post['created_by'] =auth()->user()->id;
                $save = KlasifikasiInvoice::create($post);
                $lastid_ = $save->id;
            }

            $return = array(
                'success' => "true",
                'message' => "Data berhasil di simpan.",
                'id' => $lastid_
            );
            return $return;
        } catch (Exception $e) {       // Rollback Transaction
            DB::rollback();
            $return = array(
                'success' => "false",
                'message' => "Terjadi Kesalahan",
                'id' => ""
            );
            return $return;
        }
    }

    public function show(Request $req)
    {
        $id = $req->id;
        $hu = KlasifikasiInvoice::findOrfail($id);
        if(!empty($hu)){
            $return= $hu;
        }else{
            $return= array();
        }
        return $return;
    }

    public function destroy(Request $req)
    {
        $id = $req->id;
        $delete = KlasifikasiInvoice::where('id',$id)->delete();
        if ($delete){
            $ret = "true";
            $message = 'Data berhasil di hapus.';
        }else{
            $ret = "false";
            $message = 'Data gagal di hapus. Refresh dan coba kembali. Jika masih error hubungi Administrator.';
        }
        $return = array(
            'success' => $ret,
            'message' => $message
        );
        return $return;
    }
}
