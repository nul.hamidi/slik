<?php

namespace App\Http\Controllers\Utility;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ReportController extends Controller
{

    public function export_validasi_ojk(){
        $page_title = 'Data Agunan';
        return view('pages.utility.export_validasi_ojk',compact('page_title'));
    }
}
