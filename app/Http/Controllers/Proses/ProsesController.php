<?php

namespace App\Http\Controllers\Proses;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Maatwebsite\Excel\Facades\Excel;
use DataTables;


class ProsesController extends Controller
{
    public function __construct() {
        date_default_timezone_set('asia/jakarta');
    }
    public function index()
    {
        
        $page_title = 'GANTI BULAN DATA';
        $page_description = '';
    
        return view('pages.process.gantibulan', compact('page_title', 'page_description'));
    }

    public function pencairanBaru()
    {
        
        $page_title = 'PROSES DEBITUR PENCAIRAN BARU';
        $page_description = '';
    
        return view('pages.process.pencairan_baru', compact('page_title', 'page_description'));
    }

    public function updateRincianKredit()
    {
        
        $page_title = 'PROSES UPDATE RINCIAN KREDIT';
        $page_description = '';
    
        return view('pages.process.rincian_kredit', compact('page_title', 'page_description'));
    }

    public function checkDataBelumLapor()
    {
        
        $page_title = 'CEK JIKA ADA DATA BELUM LAPOR (DBL)';
        $page_description = '';
    
        return view('pages.process.cek_belum_lapor', compact('page_title', 'page_description'));
    }

    public function generateFiletext()
    {
        
        $page_title = 'PEMBENTUKAN FILE TEXT (.txt)';
        $page_description = '';
    
        return view('pages.process.export_filetext', compact('page_title', 'page_description'));
    }

    




}
